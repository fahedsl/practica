package basicos;

class Enum{
    enum Dia { lunes, martes, miercoles, jueves, viernes, sabado, domingo};

    public static void main(String[] args){
        Enum miEnum = new Enum();

        Dia dia = Dia.martes;
        String hoy = "viernes";
        System.out.println("el dia es " + dia + " y hoy es " + hoy + '.');

        miEnum.esIgual(hoy, dia);

        hoy = "martes";
        System.out.println("el dia es " + dia + " y hoy es " + hoy + '.');
        
        miEnum.esIgual(hoy, dia);
    }

    void esIgual(String hoy, Dia dia){
        if(Dia.valueOf(hoy) == dia){
            System.out.println("igual");
        }
        else{
            System.out.println("diferente");
        }
    }
}