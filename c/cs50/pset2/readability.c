#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <cs50.h>

float calcularLiau(string entrada, int entradaTamano);
string seleccionarGrado(float liau);

int main(void)
{
    //leer entrada
    string entrada = get_string("Texto: ");
    int entradaTamano = strlen(entrada);

    //calcular Liau
    float liau = calcularLiau(entrada, entradaTamano);

    //devolver el grado
    printf("%s\n", seleccionarGrado(liau));
}


float calcularLiau(string entrada, int entradaTamano)
{
    int letras = 0;
    int palabras = 0;
    int oraciones = 0;
    float respuesta = 0.0;
    float L = 0.0;
    float S = 0.0;

    //contar letras, palabras y oraciones;
    palabras++;
    for (int i = 0; i < entradaTamano; i++)
    {
        if (isalpha(entrada[i]))
        {
            letras++;
        }

        if (entrada[i] == ' ')
        {
            palabras++;
        }

        if (entrada[i] == '.' || entrada[i] == '?' || entrada[i] == '!')
        {
            oraciones++;
        }
    }

    //ecuacion
    L = 1.0 * letras / palabras * 100;
    S = 1.0 * oraciones / palabras * 100;
    respuesta = (0.0588 * L) - (0.296 * S) - 15.8;

    return respuesta;
}


string seleccionarGrado(float liau)
{
    string grado;
    char gradoTemp[15];

    if (liau < 1)
    {
        strcpy(gradoTemp, "Before Grade 1");
    }
    else if (liau >= 1 && liau <= 16)
    {
        char temp[15];
        sprintf(temp, "%i", (int) roundf(liau));
        strcpy(gradoTemp, "Grade ");
        strcat(gradoTemp, temp);
    }
    else
    {
        strcpy(gradoTemp, "Grade 16+");
    }

    grado = gradoTemp;
    return grado;
}