import os
import sqlite3
from flask import Flask, flash, redirect, render_template, request, session
from flask_session import Session
from tempfile import mkdtemp
from werkzeug.exceptions import default_exceptions, HTTPException, InternalServerError
from werkzeug.security import check_password_hash, generate_password_hash
from helpers import apology, login_required, lookup, usd, dobleCero

app = Flask(__name__)
app.config["TEMPLATES_AUTO_RELOAD"] = True
@app.after_request
def after_request(response):
    response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    response.headers["Expires"] = 0
    response.headers["Pragma"] = "no-cache"
    return response
app.jinja_env.filters["usd"] = usd
app.jinja_env.filters["dobleCero"] = dobleCero
app.config["SESSION_FILE_DIR"] = mkdtemp()
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
Session(app)

db = sqlite3.connect("finance.db")
if not os.environ.get("API_KEY"):
    raise RuntimeError("API_KEY not set")


@app.route("/")
@login_required
def index():
    uid = session['user_id']
    shares = 0

    query = f"SELECT cash FROM users WHERE id={uid}"
    user_balance = int(dbSelectX(query)[0]['cash'])

    query = f"SELECT * FROM portfolio WHERE user_id={uid}"
    holdings = dbSelectX(query)
    for holding in holdings:
        holding['price'] = lookup(holding['symbol'])['price']
        holding['total'] = int(holding['price']) * int(holding['shares'])
        shares += holding['total']

    user = {}
    user['balance'] = user_balance
    user['total'] = user_balance + shares

    return render_template("index.html", holdings=holdings, user=user)


@app.route("/buy", methods=["GET", "POST"])
@login_required
def buy():
    if request.method == "POST":
        uid = session['user_id']

        symbol = lookup(request.form.get("symbol"))
        if symbol == None:
            return apology('Not valid.')
        symbol = symbol["symbol"]

        shares = request.form.get("shares").split(".")[0]
        fraction = request.form.get("shares").split(".")[1]
        if not shares.isnumeric():
            return apology("Shares must be numbers.")
        if int(fraction) != 0:
            return apology("Shares must be integers.")
        shares = int(shares)
        if shares < 1:
            return apology("Shares must be positive.")

        query = f"SELECT cash FROM users WHERE id={uid}"
        balance = dbSelectX(query)[0]["cash"]

        share_price = lookup(symbol)['price']
        cost = int(shares) * int(share_price)

        if int(balance) < int(cost):
            return apology("Not enough money")

        query = f"UPDATE users SET cash={(balance - cost)} WHERE id={uid}"
        dbquery(query)

        query = f"SELECT shares FROM portfolio WHERE user_id={uid} AND symbol='{symbol}'"
        current_shares = dbSelectX(query)
        if len(current_shares) > 0:
            current_shares = dbSelectX(query)[0]['shares']
            query = f"UPDATE portfolio SET shares={str(int(current_shares) + int(shares))} WHERE symbol='{symbol}' AND user_id={uid}"
        else:
            query = f"INSERT INTO portfolio (user_id, symbol, shares) VALUES ({uid}, '{symbol}', {shares})"
        dbquery(query)

        query = f"INSERT INTO transactions (user_id, type, symbol, price, shares) VALUES ({uid}, 'in', '{symbol}', {share_price}, {shares})"
        dbquery(query)

        return redirect('/history')

    return render_template("buy.html")


@app.route("/sell", methods=["GET", "POST"])
@login_required
def sell():
    if request.method == "POST":
        uid = session['user_id']
        symbol = request.form.get("symbol").upper()
        shares = request.form.get("shares").split(".")[0]
        if not shares.isnumeric():
            return apology("Shares must be integers.")
        shares = int(shares)

        query = f"SELECT symbol, shares FROM portfolio WHERE user_id={uid} and symbol='{symbol}'"
        holdings = dbSelectX(query)

        if len(holdings) <= 0:
            return apology("Must 'have' shares")

        current_shares = holdings[0]['shares']

        if current_shares < shares:
            return apology("Not enough shares of this type")

        share_price = lookup(symbol)['price']

        query = f"SELECT cash FROM users WHERE id={uid}"
        current_balance = dbSelectX(query)[0]['cash']
        current_balance = current_balance + (share_price * shares)

        query = f"UPDATE users SET cash={current_balance} WHERE id={uid}"
        dbquery(query)

        query = f"UPDATE portfolio SET shares={(current_shares - shares)} WHERE symbol='{symbol}' AND user_id={uid}"
        dbquery(query)

        query = f"INSERT INTO transactions (user_id, type, symbol, price, shares) VALUES ({uid}, 'out', '{symbol}', {share_price}, {shares})"
        dbquery(query)

        return redirect('/history')

    return render_template("sell.html")


@app.route("/quote", methods=["GET", "POST"])
@login_required
def quote():
    if request.method == "POST":
        stock = lookup(request.form.get("symbol"))
        if stock == None:
            return apology('Not valid.')

        return render_template("quoted.html", stock=stock)

    return render_template("quote.html")


@app.route("/history")
@login_required
def history():
    uid = session['user_id']
    query = f"SELECT * FROM transactions where user_id={uid}"
    transactions = dbSelectX(query)

    return render_template("history.html", transactions=transactions)


@app.route("/register", methods=["GET", "POST"])
def register():
    if request.method == 'POST':
        newuserCash = 10000
        username = request.form.get('username')
        password = request.form.get('password')
        confirmation = request.form.get('confirmation')

        if username == '':
            return apology('Username not valid.')
        query = f"SELECT username FROM users where username ='{username}'"
        if len(dbSelectX(query)) > 0:
            return apology('User already exists.')
        if password != confirmation:
            return apology('Password and confirmation don\'t match')
        if len(request.form.get('password')) < 8:
            return apology('Password must be 8 characters or longer.')


        query = f"INSERT INTO users (username, hash, cash) VALUES ('{username}','{generate_password_hash(password)}', '{newuserCash}')"
        usuario = dbquery(query)

        session['user_id'] = usuario
        return redirect('/')

    return render_template("register.html")


@app.route("/login", methods=["GET", "POST"])
def login():
    session.clear()

    if request.method == "POST":
        if not request.form.get("username"):
            return apology("must provide username")
        elif not request.form.get("password"):
            return apology("must provide password")

        name = request.form.get('username')
        query = f"SELECT * FROM users WHERE username = \'{name}\'"
        rows = dbSelectX(query)

        if len(rows) != 1 or not check_password_hash(rows[0]['hash'], request.form.get("password")):
                return apology("Error on User/Pass")

        session['user_id'] = rows[0]['id']

        return redirect("/")

    else:
        return render_template("login.html")


@app.route("/logout")
def logout():
    session.clear()
    return redirect("/")


def dbquery(query):
    con = sqlite3.connect('finance.db')
    cur = con.cursor()
    cur.execute(query)
    con.commit()
    return cur.lastrowid


def dbSelectX(query):

    def dict_factory(cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    con = sqlite3.connect('finance.db')
    con.row_factory = dict_factory
    cur = con.cursor()
    cur.execute(query)
    filas = cur.fetchall()
    return filas


def errorhandler(e):
    if not isinstance(e, HTTPException):
        e = InternalServerError()
    return apology(e.name, e.code)


for code in default_exceptions:
    app.errorhandler(code)(errorhandler)