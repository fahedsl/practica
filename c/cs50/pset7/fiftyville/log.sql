-- KEEP A LOG OF ANY SQL QUERIES YOU EXECUTE AS YOU SOLVE THE MYSTERY.

--OBTENER ESTRUCTURA DE LA INFORMACION
.schema

--SABER EL REPORTE DE LOS CRIMENES
select *
from  crime_scene_reports as r
where year = 2020
	and month = 7
	and day = 28
	and street = 'Chamberlin Street';

/*
295|2020|7|28|Chamberlin Street|Theft of the CS50 duck took place at 10:15am at the Chamberlin Street courthouse.
Interviews were conducted today with three witnesses who were present at the time — 
each of their interview transcripts mentions the courthouse.
*/

-- SABER EL CONTENIDO DE LAS ENTREVISTAS MENCIONADAS.
select *
from interviews as i
where transcript like ('%courthouse%')
	and year = 2020
	and month >= 7;

/*
161|Ruth|2020|7|28|Sometime within ten minutes of the theft, I saw the thief get into a car in the courthouse parking lot and drive away.
	If you have security footage from the courthouse parking lot, you might want to look for cars that left the parking lot in that time frame.
162|Eugene|2020|7|28|I don't know the thief's name, but it was someone I recognized. Earlier this morning, before I arrived at the courthouse,
	I was walking by the ATM on Fifer Street and saw the thief there withdrawing some money.
163|Raymond|2020|7|28|As the thief was leaving the courthouse, they called someone who talked to them for less than a minute.
	In the call, I heard the thief say that they were planning to take the earliest flight out of Fiftyville tomorrow.
	The thief then asked the person on the other end of the phone to purchase the flight ticket.
*/

--GARAJE
select p.id
	,p.name
from courthouse_security_logs as sl
join people as p
	on sl.license_plate = p.license_plate
where year = 2020
	and sl.month = 7
	and sl.day = 28
	and sl.hour = 10
	and sl.minute >= 15
	and sl.minute <= 30
	and sl.activity = 'exit';

--LLAMADA
select tel.caller, p.name
from phone_calls as tel
join people as p
	on tel.caller = p.phone_number
where duration <= 60
	and tel.year = 2020
	and tel.month = 7
	and tel.day = 28;

--ATM
select p.id
from atm_transactions as atm
join bank_accounts as ba
	on atm.account_number = ba.account_number
join people as p
	on ba.person_id = p.id
where year = 2020
	and atm.month = 7
	and atm.day = 28
	and atm.transaction_type = 'withdraw'
	and atm.atm_location = 'Fifer Street';


--GARAJE + TRANSACCIONES EN ATM + LLAMADA

select distinct p.id, p.name
from people as p
where p.id in (
select p.id
from courthouse_security_logs as sl
join people as p
	on sl.license_plate = p.license_plate
where year = 2020
	and sl.month = 7
	and sl.day = 28
	and sl.hour = 10
	and sl.minute >= 15
	and sl.minute <= 30
	and sl.activity = 'exit'
)
	and p.id in (
select p.id
from atm_transactions as atm
join bank_accounts as ba
	on atm.account_number = ba.account_number
join people as p
	on ba.person_id = p.id
where year = 2020
	and atm.month = 7
	and atm.day = 28
	and atm.transaction_type = 'withdraw'
	and atm.atm_location = 'Fifer Street'
)
	and p.id in (
select p.id
from phone_calls as tel
join people as p
	on tel.caller = p.phone_number
where duration <= 60
	and tel.year = 2020
	and tel.month = 7
	and tel.day = 28);

/*
514354|Russell
686048|Ernest

(514354, 686048)
*/

-- A QUIENES LLAMARON LOS SOSPECHOSOS

select p.id, p.name
from people as p
where p.phone_number in(
select tel.receiver
from people as p
join phone_calls as tel
	on tel.caller = p.phone_number
where tel.year = 2020
	and tel.month = 7
	and tel.day = 28
	and tel.duration <= 60
	and p.id in (514354, 686048)
);

/*
847116|Philip
864400|Berthold
*/

--ORDEN DE VUELOS

select id, hour, minute
from flights as f
where f.year = 2020
	and f.month = 7
	and f.day = 29
order by hour, minute;

/*
36|8|20 	<---------
43|9|30
23|12|15
53|15|20
18|16|0
*/

--QUE SOSPECHOSO VIAJO EN EL vuelo 36 de 2020-08-29 / CULPABLE

select pe.id, pe.name
from people as pe
join passengers as pa
	on pe.passport_number = pa.passport_number
join flights as f
	on f.id = pa.flight_id
where f.id = 36
	and f.year = 2020
	and f.month = 7
	and f.day = 29
	and pa.passport_number in
(
select pe.passport_number
from people as pe
where pe.id in (514354, 686048)
);


/*
686048|Ernest 	<------ CULPABLE
*/

--QUIEN LO AYUDÓ
select p.id, p.name
from people as p
where p.phone_number in(
select tel.receiver
from people as p
join phone_calls as tel
	on tel.caller = p.phone_number
where tel.year = 2020
	and tel.month = 7
	and tel.day = 28
	and tel.duration <= 60
	and p.id = 686048
);

/*
864400|Berthold 	<------ COMPLICE
*/

select city
from airports as a
join flights as f
	on f.destination_airport_id = a.id
where f.id = 36;

/*
London 	<------ DESTINY
*/