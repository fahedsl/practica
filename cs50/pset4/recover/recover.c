#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef uint8_t BYTE;

int main(int argc, char *argv[])
{
    int retorno = 0;

    if (argc != 2)
    {
        fprintf(stderr, "Uso: recover [entrada]\n");
        retorno = 1;
    }

    if (retorno == 0)
    {
        int conteo = 0;
        int entradaTamano = 512;
        int salidaNombreTamano = 9;
        char* entradaNombre = argv[1];
        char salidaArchivoNombre[salidaNombreTamano];
        FILE* entrada = NULL;
        FILE* salida = NULL;
        BYTE temp[entradaTamano];

        entrada = fopen(entradaNombre, "r");
        if (entrada == NULL)
        {
            printf("No se pudo abrir %s.\n", entradaNombre);
            retorno = 2;
        }

        if (retorno == 0)
        {
            while(fread(&temp, entradaTamano, 1, entrada) == 1)
            {
                if (retorno == 0)
                {
                    if (temp[0] == 0xff && temp[1] == 0xd8 && temp[2] == 0xff && (temp[3] & 0xf0) == 0xe0)
                    {
                        //Si hay salida abierta
                        if (conteo != 0)
                        {
                            fclose(salida);
                        }

                        snprintf(salidaArchivoNombre, salidaNombreTamano, "%03i.jpg",  conteo);
                        salida = fopen(salidaArchivoNombre, "w");
                        if (salida == NULL)
                        {
                            printf("No se pudo crear %s.\n", salidaArchivoNombre);
                            retorno = 3;
                        }

                        conteo++;
                    }
                }

                if (retorno == 0)
                {
                    if (entrada != NULL && salida != NULL)
                    {
                        fwrite(&temp, entradaTamano, 1, salida);
                    }
                }
            }
        }
        if (entrada != NULL)
        {
            fclose(entrada);
        }
        if (salida != NULL)
        {
            fclose(salida);
        }
    }

    return retorno;
}