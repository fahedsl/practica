from csv import reader, DictReader
from sys import argv, exit


def main():
    retorno = 0
    resultado = "No match"
    secuencias = {}

    if len(argv) != 3:
        print("Usage:", "python dna.py data.csv sequence.txt")
        retorno = 1
    else:
        dnaArchivo = open(argv[2], 'r')
        dna = dnaArchivo.read()

        # open peopleFile to get sequence keyes
        personasArchivo = open(argv[1], "r")
        personasLector1 = reader(personasArchivo)
        for row in personasLector1:
            header = row
            header.pop(0)
            for item in header:
                secuencias[item] = 0
            break

        for key in secuencias:
            secuencias[key] = obtenerMaximo(dna, key)

        personasArchivo.seek(0)
        personasLector1 = DictReader(personasArchivo)
        for persona in personasLector1:
            match = 0
            for key in secuencias:
                if int(persona[key]) == secuencias[key]:
                    match += 1
            if match == len(secuencias):
                resultado = persona["name"]
                break

    print(resultado)
    return retorno


def obtenerMaximo(dna, STR):
    retorno = 0
    STRlen = len(STR)
    i = 0
    j = STRlen
    for x in range(len(dna)):
        if dna[i:j] == STR:
            temp = 0
            while dna[i:j] == STR:
                temp += 1
                i += STRlen
                j += STRlen
                if(temp > retorno):
                    retorno = temp
        else:
            i += 1
            j += 1

    return retorno


main()