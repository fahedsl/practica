#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <cs50.h>

void limpiarArregloChar(unsigned char arreglo[], int tamano);
void limpiarArregloInt(int arreglo[], int tamano);
int calcularArregloContenidoTamano(unsigned char arreglo[], int tamano);
string validarTarjeta(int tarjeta[], int tamano);
bool validarEntrada(unsigned char entrada[], int tamano);
void convertirArregloCharAInt(unsigned char entradaChar[], int entradaInt[], int tamano);
bool pruebaLuhn(int numero[], int tamano);

int main(void)
{
    //Variables
    string validacion;
    bool entradaValida = false;
    int tarjetaNumeroTamanoMax = 20;
    int tarjetaNumeroTamanoLeido = 0;
    int tarjetaNumero[tarjetaNumeroTamanoMax];
    limpiarArregloInt(tarjetaNumero, tarjetaNumeroTamanoMax);

    //Lectura y validacion de entrada
    while (entradaValida == false)
    {
        unsigned char entradaChar[tarjetaNumeroTamanoMax];
        limpiarArregloChar(entradaChar, tarjetaNumeroTamanoMax);
        int entradaInt[tarjetaNumeroTamanoMax];
        limpiarArregloInt(entradaInt, tarjetaNumeroTamanoMax);
        int entradaTamanoLeido;

        printf("Ingrese su número de tarjeta: ");
        scanf("%s", entradaChar);
        //printf("leido: %s\n", entradaChar);

        entradaTamanoLeido = calcularArregloContenidoTamano(entradaChar, tarjetaNumeroTamanoMax);
        entradaValida = validarEntrada(entradaChar, entradaTamanoLeido);
        if (entradaValida == true)
        {
            tarjetaNumeroTamanoLeido = entradaTamanoLeido;
            convertirArregloCharAInt(entradaChar, entradaInt, entradaTamanoLeido);
            for (int i = 0; i < tarjetaNumeroTamanoLeido; i++)
            {
                tarjetaNumero[i] = entradaInt[i];
            }
        }
    }

    //Validacion de tarjeta
    validacion = validarTarjeta(tarjetaNumero, tarjetaNumeroTamanoLeido);
    printf("%s", validacion);
}


string validarTarjeta(int tarjeta[], int tamano)
{
    string retorno = "INVALID\n";

    if (tamano == 13 || tamano == 15 || tamano == 16) //Verifica longitud de numeros usados por las empresas
    {
        bool luhn = pruebaLuhn(tarjeta, tamano);

        if (luhn == true)
        {
            //VISA
            if (tarjeta[0] == 4)
            {
                retorno = "VISA\n";
            }
            //MC
            else if (tarjeta[0] == 5 && tarjeta[1] > 0 && tarjeta[1] < 6)
            {
                retorno = "MASTERCARD\n";
            }
            //AMEX
            else if (tarjeta[0] == 3 && (tarjeta[1] == 4 || tarjeta[1] == 7))
            {
                retorno = "AMEX\n";
            }
        }
    }

    return retorno;
}


bool validarEntrada(unsigned char entrada[], int tamano)
{
    bool esValido = true;

    for (int i = 0; i < tamano; i++)
    {
        if (!isdigit(entrada[i]))
        {
            esValido = false;
            break;
        }
    }

    return esValido;
}


void limpiarArregloChar(unsigned char arreglo[], int tamano)
{
    for (int i = 0; i < tamano; i++)
    {
        arreglo[i] = '\0';
    }

    return;
}


void limpiarArregloInt(int arreglo[], int tamano)
{
    for (int i = 0; i < tamano; i++)
    {
        arreglo[i] = 0;
    }

    return;
}


void convertirArregloCharAInt(unsigned char entradaChar[], int entradaInt[], int tamano)
{
    for (int i = 0; i < tamano; i++)
    {
        entradaInt[i] = entradaChar[i] - '0';
    }
}


int calcularArregloContenidoTamano(unsigned char arreglo[], int tamano)
{
    int arregloContenidoTamano = 0;

    for (int i = 0; i < tamano; i++)
    {
        if (arreglo[i] != '\0')
        {
            arregloContenidoTamano++;
        }
    }

    return arregloContenidoTamano;
}


bool pruebaLuhn(int numero[], int tamano)
{
    bool retorno = false;
    int sumatoria = 0;

    //pares
    for (int i = tamano - 2; i >= 0; i -= 2)
    {
        int temp = numero[i] * 2;
        if (temp >= 10)
        {
            temp = (temp % 10) + 1;
        }
        sumatoria += temp;
    }

    //impares
    for (int i = tamano - 1; i >= 0; i -= 2)
    {
        sumatoria += numero[i];
    }

    //ultimo digito
    if (sumatoria % 10 == 0)
    {
        retorno = true;
    }

    return retorno;
}