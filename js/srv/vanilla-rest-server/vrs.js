'use strict';

import * as http from 'http';
import * as datos from './vrs-demo-datos.json';

//console.log(datos);

const servidor = http.createServer(function servidorCrear(req, res){
	res.statusCode = 200;
	//res.setHeader('Content-Type','text/html');
	res.setHeader('Content-Type','application/json');
	res.write(JSON.stringify(datos));
	res.end();
});
const puerto = process.env.PORT || 5001;
servidor.listen(puerto, ()=>{console.log(`servidor en ${puerto}`);});
