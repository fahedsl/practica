// Implements a dictionary's functionality

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>
#include <strings.h>

#include "dictionary.h"

// Represents a node in a hash table
typedef struct node
{
    char word[LENGTH + 1];
    struct node *next;
}
node;

// Number of buckets in hash table
const unsigned int N = 10;

// Hash table
node *table[N];
int diccTamano = 0;

// Returns true if word is in dictionary, else false
bool check(const char *word)
{
    bool retorno = false;

    int hash1 = hash(word);
    node *nodo = table[hash1];
    while (nodo != NULL)
    {
        if (strcasecmp(word, nodo->word) == 0)
        {
            retorno = true;
            break;
        }
        nodo = nodo->next;
    }
    return retorno;
}

// Hashes word to a number
unsigned int hash(const char *word)
{
    int retorno = 0;

    const char *palabra = word;
    int palabraTamano = strlen(palabra);

    for (int i = 0; i < palabraTamano; i++)
    {
        retorno += tolower(palabra[i]);
    }

    retorno = retorno % N;

    return retorno;
}

// Loads dictionary into memory, returning true if successful else false
bool load(const char *dictionary)
{
    bool retorno = true;

    FILE *diccionario = fopen(dictionary, "r");
    if (diccionario != NULL)
    {
        char palabra[LENGTH + 1];
        while (fscanf(diccionario, "%s", palabra) != EOF)
        {
            node *nodo = malloc(sizeof(node));
            if (nodo != NULL)
            {
                int hash1 = hash(palabra);
                strcpy(nodo->word, palabra);
                nodo->next = table[hash1];
                table[hash1] = nodo;
                diccTamano++;
            }
            else
            {
                retorno = false;
            }
        }

        fclose(diccionario);
    }
    else
    {
        retorno = false;
    }

    return retorno;
}

// Returns number of words in dictionary if loaded, else 0 if not yet loaded
unsigned int size(void)
{
    return diccTamano;
}

// Unloads dictionary from memory, returning true if successful, else false
bool unload(void)
{
    bool retorno = false;

    for (int i = 0; i < N; i++)
    {
        node *nodo = table[i];

        while (nodo != NULL)
        {
            node *tmp = nodo;
            nodo = nodo->next;
            free(tmp);
        }
        if (nodo == NULL && i == N - 1)
        {
            retorno = true;
        }
    }

    return retorno;
}