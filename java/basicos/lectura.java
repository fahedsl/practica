package basicos;

import java.util.Scanner;
import java.io.BufferedReader;
import java.io.InputStreamReader;


class lectura{

	public static void main(String[] args){
		String entradaStr;
		
		System.out.print("Console	- Escriba: ");
		entradaStr = leeConsole.leer();

		System.out.print("Scanner	- Escriba: ");
		entradaStr = leeScanner.leer();

		System.out.print("BufferR	- Escriba: ");
		entradaStr = leeBufferR.leer();
	}
}

class leeConsole{
	static String leer(){
		String lectura = "";
		System.console().readLine();
		return lectura;
	}	
}

class leeScanner{
	static String leer(){
		String lectura = "";
		System.console().readLine();
		return lectura;
	}
}

class leeBufferR{
	static String leer(){
		String lectura = "";
		BufferedReader reader = 
				new BufferedReader(new InputStreamReader(System.in));
			String name = reader.readLine();
			System.out.println(name); 
		return lectura;
	}
}