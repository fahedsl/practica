Elemento más largo de una lista.

Invierte la lista, preferblemente sobre si misma

Verifica si un elemento esta en una lista.

Elementos en posiciones impares de la lista.

Total de elemetos de una  lista.

Verifica una palabra es reversible.

Suma elementos de una lista, 1 for, 1 while y recursividad.

Pide numero, devuelve los siguientes 5 cuadrados perfectos.

Encadena 2 listas.

Encadena 2 listas, alternando.

Encadena y ordena. You can do this quicker.

Pide número. Rota una lista esa cantidad de elementos. SIN PUNTEROS.

Pide número. Rota una lista esa cantidad de elementos. CON PUNTEROS.

Lista 100 primeros Fibonacci.

Pide número. Hace lista de sus digitos.

Implementa busqueda binaria.

Imprime lista dentro de cuadrito.

Ordenar por SELECTION SORT.

Ordenar por INSERTION SORT.

Ordenar por MERGE SORT.

Ordenar por QUICK SORT.

Ordenar por STOOGE SORT.

Traduce a PIG LATIN.

Write functions that add, subtract, and multiply two numbers in their
digit-list representation (and return a new digit list). If you’re
ambitious you can implement Karatsuba multiplication. Try different
bases. What is the best base if you care about speed? If you couldn’t
completely solve the prime number exercise above due to the lack of
large numbers in your language, you can now use your own library for
this task.

Write a function that takes a list of numbers, a starting base b1 and
a target base b2 and interprets the list as a number in base b1 and
converts it into a number in base b2 (in the form of a list-of-digits).

