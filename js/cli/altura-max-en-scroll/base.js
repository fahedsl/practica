window.addEventListener('resize', ()=>{laFuncion(['.woocommerce ul.products.columns-3 li'], ['.woocommerce ul.products.columns-3'], 100)});
document.addEventListener('scroll', ()=>{laFuncion(['.woocommerce ul.products.columns-3 li'], ['.woocommerce ul.products.columns-3'], 100)});

function laFuncion(a, b, extra){
	igualarAlturasAlContenido(a, b, extra);
}

//VERIFICACION
function esPrimeraEjecucion(id = 'marca'){
	let retorno = true;
	if(document.getElementById(id) == null){
		let marca = document.createElement('div');
		marca.setAttribute('id', id);
		marca.setAttribute('style', 'display:none');
		document.body.appendChild(marca);
	}
	else{
		retorno = false;
	}
	return retorno;
}

//ALTURAS
function igualarAlturasAlMax(a, b){
	let origen = seleccionarElemento(a);
	let destino = seleccionarElemento(b);
	let altura = encontrarAlturaMax(origen);
	establecerAltura(destino, altura);
}

function igualarAlturasAlContenido(a, b, extra){
	let origen = seleccionarElemento(a);
	let destino = seleccionarElemento(b);
	let altura = encontrarAlturaMaxTotalContenido(origen, extra);
	establecerAltura(destino, altura);
}

function seleccionarElemento(selectores){
	let retorno = [];
	for(i = 0; i < selectores.length; i++){
		let elementos = document.querySelectorAll(selectores[i]);
		if (elementos != null) {
 			for(j = 0; j < elementos.length; j++){
				retorno.push(elementos[j]);
			}
		}
		else{
		}
	}
	return retorno
}

function encontrarAlturaMax(elementos){
	let retorno = 0;
	for(i = 0; i < elementos.length; i++){
		if(elementos[i].clientHeight > retorno){
			retorno = elementos[i].clientHeight;
		}
	}
	return retorno;
}

function encontrarAlturaMaxTotalContenido(elementos, extra){
	let retorno = 0;
	for(let i = 0; i < elementos.length; i++){
		let hijos = Array.prototype.slice.call(elementos[i].children)
		var altura = 0;
		for(let j = 0; j < hijos.length; j++){
			altura = altura + hijos[j].clientHeight;
		}
		if(retorno < altura){
			retorno = altura;
		}
	}
	retorno = retorno + extra;
	return retorno;
}

function establecerAltura(elementos, altura){
	for(i = 0; i < elementos.length; i++){
		elementos[i].setAttribute('style', 'height:'+altura+'px;');
		elementos[i].style.height = altura;
	}
}
