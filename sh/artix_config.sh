https://www.youtube.com/watch?v=XO5LTmIhUTs
USU: artix
PAS: artix

su

#RED
ip a #debe mostrar las ip
#ethernet
s6-rc -u change dhcpcd-srv
#wifi
s6-rc -u change connmand-srv
rfkill unblock wifi
ip link set wlan0 up
connmanctl
	> scan wifi
	> services
	> agent on
	> connect wifi...

#PARTICIONES
lsblk
cfdisk
	> 1 UEFI 200MB
	> 1 LFS resto
mkfs.fat -F32 /dev/{la-unidad-LFS}
mnt /dev/{la-unidad-LFS} /mnt
mkdir -p /mnt/boot/efi
mount /dev/{la-unidad-EFI} /mnt/boot/efi


#SISTEMA BASE
basestrap /mnt base base-devel s6 linux linux-firware nano amd-ucode
fstabgen -U /mnt >> /mnt/etc/fstab
artools-chroot /mnt #con esto se cambia al sistema, no en el ISO
