print("Número (0-9):")
entrada = input()
while not entrada.isnumeric() or int(entrada) < 1 or int(entrada) > 8:
    print("Número (0-9):")
    entrada = input()
entrada = int(entrada) + 1

for i in range(1, entrada):
    texto = ' ' * (entrada - i - 1) + '#' * (i) + '  ' + '#' * (i)
    print(texto)