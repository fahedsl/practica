#!/bin/bash

###GENERAL###

#BORRAR
	apt purge -y catfish
	apt purge -y mousepad
	apt purge -y xfce4-notes
	apt purge -y xfce4-notes-plugin
	apt purge -y xfce4-weather-plugin
	apt purge -y xfce4-places-plugin
	apt purge -y sgt-launcher
	apt purge -y sgt-puzzles
	apt purge -y gnome-mines
	apt purge -y gnome-sudoku
	apt purge -y pidgin
	apt purge -y anydesk
	apt purge -y parole
	apt purge -y firefox
	apt purge -y libreoffice
	apt purge -y xfce4-terminal
	apt purge -y xterm
	apt purge -y stterm
	apt purge -y xfce4-screenshooter
	apt-get purge -y blue*
	rm -r /home/f/Documents
	rm -r /home/f/Music
	rm -r /home/f/Pictures
	rm -r /home/f/Public
	rm -r /home/f/Templates
	rm -r /home/f/Videos

	apt update
	apt upgrade -y
	apt install -y git
	apt install -y snap
	apt install -y snapd
	apt install -y wget
	apt install -y curl
	apt install -y zip
	apt install -y unzip
	apt install -y rar
	apt install -y unrar
	apt install -y inkscape
	apt install -y gimp
	apt install -y vlc
	apt install -y firefox
	apt install -y chromium-browser
	apt install -y libreoffice
	apt install -y ffmpeg
	apt install -y libavcodec-extra-53
	apt install -y obs-studio
	apt install -y kdenlive
	apt install -y flameshot
	apt install -y transmission
	apt install -y neomutt
	apt install -y audacity
	apt install -y fbreader
	#terminal
	add-apt-repository ppa:mmstick76/alacritty
	apt-get update
	apt install -y alacritty
	#spotify
	curl -sS https://download.spotify.com/debian/pubkey_0D811D58.gpg | apt-key add -
	echo "deb http://repository.spotify.com stable non-free" | tee /etc/apt/sources.list.d/spotify.list
	apt-get update
	apt-get install -y spotify-client
	#gammy
	apt install -y build-essential
	apt install -y libgl1-mesa-dev
	apt install -y libxext-dev
	apt install -y libxxf86vm-dev
	apt install -y qt5-default
	git clone https://github.com/Fushko/gammy.git
	mv gammy /opt/gammy
	cd /opt/gammy
	qmake Gammy.pro
	make install
	cat > /home/$USER/.config/autostart/gammy.desktop <<EOL
[Desktop Entry]
Type=Application
Exec=/opt/gammy/gammy
Hidden=false
NoDisplay=false
X-GNOME-Autostart-enabled=true
Name=Gammy adaptive screen brightness
EOL
#chats
	snap install core
	#telegram
	snap install telegram-desktop
	#whatsapp
	snap install whatsdesk
#remoto
	#zoom
	wget https://zoom.us/client/latest/zoom_amd64.deb -O /tmp/zoom.deb
	apt install -y /tmp/zoom.deb
	rm -rf /tmp/zoom.deb
	#teamviewer
	wget https://download.teamviewer.com/download/linux/teamviewer_amd64.deb -O /tmp/teamviewer.deb
	apt install -y /tmp/teamviewer.deb
	rm -rf /tmp/teamviewer.deb
	#anydesk
	wget -qO - https://keys.anydesk.com/repos/DEB-GPG-KEY | apt-key add -
	echo "deb http://deb.anydesk.com/ all main" > /etc/apt/sources.list.d/anydesk-stable.list
	apt update
	apt install -y anydesk
	systemctl disable anydesk.service
	#graficos
	add-apt-repository ppa:graphics-drivers/ppa
	apt update
	apt install -y nvidia-driver-460
	apt install -y nvidia-opencl-icd-340
	apt install -y libcuda1-340
	apt install -y libnvidia-encode-460


### PROGRAMACION ###

#"LENGUAJES y ENTORNOS"
#apache
	apt install -y apache2
	apt install -y libapache2-mod-fcgid
	apt install -y libapache2-mod-php
	apt install -y software-properties-common
	a2enmod actions fcgid alias proxy_fcgi rewrite
#php
	apt install -y php
	apt install -y php-cli
	apt install -y php-curl
	apt install -y php-fpm
	apt install -y php-gd
	apt install -y php-json
	apt install -y php-mbstring
	apt install -y php-mysql
	apt install -y php8.0-opcache
	apt install -y php-xml
	apt install -y php-mysql
	apt install -y php-mysqlnd-ms
	apt install -y php-pear
	phpenmod mysqli
	service apache2 restart
	chown -R currentuser:currentgroup /srv/www/htdocs/wordpress
#node.js
	curl -sL https://deb.nodesource.com/setup_current.x | sudo -E bash -
	apt install -y nodejs
	apt install -y npm
#java
	apt install -y openjdk-18-jdk
#android
	apt install -y adb
	apt install -y fastboot
	apt-key adv --keyserver keyserver.ubuntu.com --recv-keys D7CC6F019D06AF36
	add-apt-repository -y ppa:cwchien/gradle
	apt-get install gradle -y
	gradle -v
#C++
	apt install -y build-essential
	apt install -y manpages-dev
	apt install -y gcc
	apt install -y g++
#Rust
	curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
	source $HOME/.cargo/env
	rustc --version
#mysql
	apt install -y mariadb-server
	apt install -y mariadb-client
	mysql -Bse "CREATE USER 'f'@'localhost' IDENTIFIED BY '12';"
	mysql -Bse "GRANT ALL PRIVILEGES ON * . * TO 'f'@'localhost';"
	mysql -Bse "flush privileges;"
#couchdb
	sudo apt update && sudo apt install -y curl apt-transport-https gnupg
	curl https://couchdb.apache.org/repo/keys.asc | gpg --dearmor | sudo tee /usr/share/keyrings/couchdb-archive-keyring.gpg >/dev/null 2>&1
	source /etc/os-release
	echo "deb [signed-by=/usr/share/keyrings/couchdb-archive-keyring.gpg] https://apache.jfrog.io/artifactory/couchdb-deb/ ${VERSION_CODENAME} main" \
	| sudo tee /etc/apt/sources.list.d/couchdb.list >/dev/null
	sudo apt update
	sudo apt install -y couchdb
#couchbase
	pecl install https://packages.couchbase.com/clients/php/couchbase-3.1.2.tgz

#EDITORES
#sublime
	wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | apt-key add -
	echo "deb https://download.sublimetext.com/ apt/stable/" | tee /etc/apt/sources.list.d/sublime-text.list
	apt update -y
	apt install -y sublime-text
#	cat > /home/$USER/.config/sublime-text/Packages/User/Default (Linux).sublime-keymap <<EOL
#[
#	{ "keys": ["ctrl+tab"], "command": "next_view_in_stack" },
#	{ "keys": ["ctrl+shift+tab"], "command": "prev_view_in_stack" },
#]
#EOL
#VSC
	wget -O vsc.deb https://az764295.vo.msecnd.net/stable/dfd34e8260c270da74b5c2d86d61aee4b6d56977/code_1.66.2-1649664567_amd64.deb
	dpkg -i vsc.deb
	rm vsc.deb
#	cat > /home/$USER/.config/Code/User/keybindings.json <<EOL
#// Place your key bindings in this file to override the defaults
#[
#    {
#        "key": "ctrl+tab",
#        "command": "workbench.action.nextEditor"
#    },
#    {
#        "key": "ctrl+shift+tab",
#        "command": "workbench.action.previousEditor"
#    }
#]
#EOL
#Intellij Idea
	add-apt-repository ppa:mmk2410/intellij-idea
	apt update
	apt install intellij-idea-community

#HERRAMIENTAS
#Composer
	curl -sS https://getcomposer.org/installer -o composer-setup.php
	mkdir /opt/composer
	php  --install-dir=/opt/composer --filename=composer
	export PATH="${PATH:+${PATH}:}/opt/composer"
	rm composer-setup.php
#Sourcetrail
	wget -O sourcetrail.tar.gz https://github.com/CoatiSoftware/Sourcetrail/releases/download/2021.4.19/Sourcetrail_2021_4_19_Linux_64bit.tar.gz
	tar -xzvf sourcetrail.tar.gz
	rm -r sourcetrail.tar.gz
	sudo ./Sourcetrail/install.sh
	rm -r Sourcetrail

#	make install
#	cat > /home/$USER/.config/autostart/gammy.desktop <<EOL
#[Desktop Entry]
#Type=Application
#Exec=/opt/gammy/gammy
#Hidden=false
#NoDisplay=false
#X-GNOME-Autostart-enabled=true
#Name=Gammy adaptive screen brightness
#EOL

#dbeaver
	wget https://dbeaver.io/files/dbeaver-ce_latest_amd64.deb -O /tmp/dbeaver.deb
	apt install -y /tmp/dbeaver.deb
	rm /tmp/dbeaver.deb
#filezilla
	apt install -y filezilla
#httpie (apis)
	apt install -y httpie
#
#sdk
	wget https://dl.google.com/android/repository/commandlinetools-linux-6858069_latest.zip -O /tmp/android-sdk.zip
	mkdir -p /opt/android-sdk
	unzip /tmp/android-sdk.zip -d /opt/android-sdk
	rm -r /tmp/android-sdk.zip
	echo 'export ANDROID_SDK_ROOT="/opt/android-sdk"' > /etc/profile.d/android.sh
	echo 'export ANDROID_HOME="$ANDROID_SDK_ROOT"' >> /etc/profile.d/android.sh
	echo 'export PATH="$PATH:$ANDROID_SDK_ROOT/cmdline-tools/latest/bin"' >> /etc/profile.d/android.sh
	echo 'export PATH="$PATH:$ANDROID_SDK_ROOT/cmdline-tools/tools/bin"' >> /etc/profile.d/android.sh
	echo 'export PATH="$PATH:$ANDROID_SDK_ROOT/platform-tools"' >> /etc/profile.d/android.sh
	echo 'export PATH="$PATH:$ANDROID_SDK_ROOT/tools"' >> /etc/profile.d/android.sh
	export ANDROID_SDK_ROOT="/opt/android-sdk"
	cd /etc/profile.d/
	chmod +x android.sh
	./android.sh
	chmod -x android.sh
	#correccion ubicacion tools
	cd $ANDROID_SDK_ROOT
	cp -r $ANDROID_SDK_ROOT/cmdline-tools $ANDROID_SDK_ROOT/cmdline-tools2
	rm -r $ANDROID_SDK_ROOT/cmdline-tools/*
	mv $ANDROID_SDK_ROOT/cmdline-tools2 $ANDROID_SDK_ROOT/cmdline-tools/tools
	cd $ANDROID_SDK_ROOT/cmdline-tools/tools/bin
	./sdkmanager
	./sdkmanager platform-tools
	./sdkmanager "platforms;android-30"
	./sdkmanager "build-tools;30.0.3"
	./sdkmanager "cmdline-tools;latest"
	yes | ./sdkmanager --update
	yes | ./sdkmanager --licenses
	#conseguir herramienta "android"
