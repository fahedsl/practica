#!/bin/bash

###GENERAL###

#BORRAR
	apt purge -y firefox
	apt purge -y libreoffice
	apt purge -y 2048-qt
	apt purge -y noblenote
	apt purge -y vim
	apt purge -y featherpad
	apt purge -y kdeconnect
	apt purge -y qtpasskde		
	apt purge -y quassel*
	apt purge -y screengrab
	apt purge -y skanlite
	apt purge -y plasma-discover*
	apt purge -y qpdfview
	apt purge -y qtpass
	apt purge -y qlipper
	apt purge -y texinfo
	apt purge -y qpdfview
	apt-get purge -y blue*
	rm -r /home/f/Documents
	rm -r /home/f/Music
	rm -r /home/f/Pictures
	rm -r /home/f/Public
	rm -r /home/f/Templates
	rm -r /home/f/Videos

	apt install --fix-broken

	apt update
	apt upgrade -y
	apt install -y git
	apt install -y cmake
	apt install -y pkg-config
	apt install -y libfreetype6-dev
	apt install -y libfontconfig1-dev
	apt install -y libxcb-xfixes0-dev
	apt install -y libxkbcommon-dev
	apt install -y python3
	apt install -y gzip
	apt install -y snap
	apt install -y nano
	apt install -y snapd
	apt install -y wget
	apt install -y curl
	apt install -y zip
	apt install -y unzip
	apt install -y rar
	apt install -y unrar
	apt install -y inkscape
	apt install -y gimp
	apt install -y blender
	apt install -y vlc
	apt install -y firefox
	apt install -y chromium-browser
	apt install -y ffmpeg
	apt install -y libavcodec-extra-53
	apt install -y obs-studio
	apt install -y kdenlive
	apt install -y flameshot
	apt install -y transmission
	apt install -y neomutt
	apt install -y audacity
	apt install -y libreoffice
	apt install -y thunderbird
	#terminal
	git clone https://github.com/alacritty/alacritty.git
	cd alacritty
	cargo build --release
	mkdir -p /opt/alacritty
	cp target/release/alacritty /opt/alacritty/alacritty
	mv extra/linux/Alacritty.desktop /usr/share/applications/alacritty.desktop
	mv extra/logo/compat/alacritty-term.svg /opt/alacritty/alacritty.svg
	cd
	rm -r alacritty
	echo 'export ALACRITTY_PATH="/otp/alacritty"' >> /etc/profile.d/alacritty.sh
	echo 'export PATH="$PATH:$ALACRITTY_PATH"' >> /etc/profile.d/alacritty.sh
	chmod +x /etc/profile.d/alacritty.sh
	update-alternatives --config x-terminal-emulator
	#spotify
	apt install -y libcanberra-gtk-module
	apt install -y software-properties-common
	apt install -y apt-transport-https
	apt install -y gnupg2
	apt install -y ubuntu-keyring
	wget -O- https://download.spotify.com/debian/pubkey_5E3C45D7B312C643.gpg | gpg --dearmor | sudo tee /usr/share/keyrings/spotify.gpg
	echo "deb [signed-by=/usr/share/keyrings/spotify.gpg] http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list
	apt update
	apt install -y spotify-client
#chats
	snap install core
	#telegram
	snap install telegram-desktop
	#whatsapp
	snap install whatsdesk
	#discord
	wget https://dl.discordapp.net/apps/linux/0.0.18/discord-0.0.18.deb -O /tmp/discord.deb
	dpkg -i /tmp/discord.deb
	rm /tmp/discord.deb
#remoto
	#zoom
	wget https://zoom.us/client/latest/zoom_amd64.deb -O /tmp/zoom.deb
	apt install -y /tmp/zoom.deb
	rm -rf /tmp/zoom.deb
	#teamviewer
	wget https://download.teamviewer.com/download/linux/teamviewer_amd64.deb -O /tmp/teamviewer.deb
	apt install -y /tmp/teamviewer.deb
	rm -rf /tmp/teamviewer.deb
	#anydesk
	wget -qO - https://keys.anydesk.com/repos/DEB-GPG-KEY | apt-key add -
	echo "deb http://deb.anydesk.com/ all main" > /etc/apt/sources.list.d/anydesk-stable.list
	apt update
	apt install -y anydesk
	systemctl disable anydesk.service
	#graficos
	add-apt-repository ppa:graphics-drivers/ppa
	apt update
	apt install -y nvidia-driver-460
	apt install -y nvidia-opencl-icd-340
	apt install -y libcuda1-340
	apt install -y libnvidia-encode-460


### PROGRAMACION ###

#"LENGUAJES y ENTORNOS"
#apache
	apt install -y apache2
	apt install -y libapache2-mod-fcgid
	apt install -y libapache2-mod-php
	apt install -y software-properties-common
	a2enmod actions fcgid alias proxy_fcgi rewrite
#php
	apt install -y php
	apt install -y php-cli
	apt install -y php-curl
	apt install -y php-fpm
	apt install -y php-gd
	apt install -y php-json
	apt install -y php-mbstring
	apt install -y php-mysql
	apt install -y php8.0-opcache
	apt install -y php-xml
	apt install -y php-mysql
	apt install -y php-mysqlnd-ms
	apt install -y php-pear
	phpenmod mysqli
	service apache2 restart
	chown -R currentuser:currentgroup /srv/www/htdocs/wordpress
#node.js
	curl -sL https://deb.nodesource.com/setup_current.x | sudo -E bash -
	apt install -y nodejs
	apt install -y npm
#java
	apt install -y openjdk-18-jdk
#C++
	apt install -y build-essential
	apt install -y manpages-dev
	apt install -y gcc
	apt install -y g++
#Rust
	curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
	source $HOME/.cargo/env
	rustc --version
#mysql
	apt install -y mariadb-server
	apt install -y mariadb-client
	mysql -Bse "CREATE USER 'f'@'localhost' IDENTIFIED BY '12';"
	mysql -Bse "GRANT ALL PRIVILEGES ON * . * TO 'f'@'localhost';"
	mysql -Bse "flush privileges;"
#couchdb
	sudo apt update && sudo apt install -y curl apt-transport-https gnupg
	curl https://couchdb.apache.org/repo/keys.asc | gpg --dearmor | sudo tee /usr/share/keyrings/couchdb-archive-keyring.gpg >/dev/null 2>&1
	source /etc/os-release
	echo "deb [signed-by=/usr/share/keyrings/couchdb-archive-keyring.gpg] https://apache.jfrog.io/artifactory/couchdb-deb/ ${VERSION_CODENAME} main" \
	| sudo tee /etc/apt/sources.list.d/couchdb.list >/dev/null
	sudo apt update
	sudo apt install -y couchdb
#couchbase
	pecl install https://packages.couchbase.com/clients/php/couchbase-3.1.2.tgz

#EDITORES
#sublime
	wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | apt-key add -
	echo "deb https://download.sublimetext.com/ apt/stable/" | tee /etc/apt/sources.list.d/sublime-text.list
	apt update -y
	apt install -y sublime-text
#VisualStudioCode
	wget -O vsc.deb https://az764295.vo.msecnd.net/stable/dfd34e8260c270da74b5c2d86d61aee4b6d56977/code_1.66.2-1649664567_amd64.deb
	dpkg -i vsc.deb
	rm vsc.deb
#HERRAMIENTAS
#Composer
	curl -sS https://getcomposer.org/installer -o composer-setup.php
	mkdir /opt/composer
	php  --install-dir=/opt/composer --filename=composer
	export PATH="${PATH:+${PATH}:}/opt/composer"
	rm composer-setup.php
#Sourcetrail
	wget -O sourcetrail.tar.gz https://github.com/CoatiSoftware/Sourcetrail/releases/download/2021.4.19/Sourcetrail_2021_4_19_Linux_64bit.tar.gz
	tar -xzvf sourcetrail.tar.gz
	rm -r sourcetrail.tar.gz
	sudo ./Sourcetrail/install.sh
	rm -r Sourcetrail
#dbeaver
	wget https://dbeaver.io/files/dbeaver-ce_latest_amd64.deb -O /tmp/dbeaver.deb
	apt install -y /tmp/dbeaver.deb
	rm /tmp/dbeaver.deb
#filezilla
	apt install -y filezilla
#httpie (apis)
	apt install -y httpie

#android
	apt install -y adb
	apt install -y fastboot
	#gradle
	wget https://downloads.gradle-dn.com/distributions/gradle-7.5-rc-5-bin.zip -O /tmp/gradle.zip
	mkdir /opt/gradle
	unzip /tmp/gradle.zip -d /opt/gradle
	mv /opt/gradle/gradle-7.5-rc-5/* /opt/gradle
	rm -r /opt/gradle/gradle-7.5-rc-5
	rm /tmp/gradle.zip
	echo 'export GRADLE_HOME="/opt/gradle"' >> /etc/profile.d/gradle.sh
	echo 'export PATH="$PATH:$GRADLE_HOME/bin"' >> /etc/profile.d/gradle.sh
	chmod +x /etc/profile.d/gradle.sh
	gradle -v
	#sdk
	wget https://dl.google.com/android/repository/commandlinetools-linux-8512546_latest.zip -O /tmp/android-sdk.zip
	mkdir -p /opt/android-sdk/cmdline-tools
	unzip /tmp/android-sdk.zip -d /opt/android-sdk/cmdline-tools
	rm -r /tmp/android-sdk.zip
	mv /opt/android-sdk/cmdline-tools/cmdline-tools /opt/android-sdk/cmdline-tools/latest
	echo 'export ANDROID_SDK_ROOT="/opt/android-sdk"' > /etc/profile.d/android.sh
	echo 'export ANDROID_HOME="$ANDROID_SDK_ROOT"' >> /etc/profile.d/android.sh
	echo 'export PATH="$PATH:$ANDROID_SDK_ROOT/cmdline-tools/latest/bin"' >> /etc/profile.d/android.sh
	echo 'export PATH="$PATH:$ANDROID_SDK_ROOT/cmdline-tools/tools/bin"' >> /etc/profile.d/android.sh
	echo 'export PATH="$PATH:$ANDROID_SDK_ROOT/platform-tools"' >> /etc/profile.d/android.sh
	echo 'export PATH="$PATH:$ANDROID_SDK_ROOT/tools"' >> /etc/profile.d/android.sh
	chmod +x /etc/profile.d/android.sh
	sh /etc/profile.d/android.sh
	yes | /opt/android-sdk/cmdline-tools/latest/bin/sdkmanager "platform-tools"
	yes | /opt/android-sdk/cmdline-tools/latest/bin/sdkmanager "platforms;android-30"
	yes | /opt/android-sdk/cmdline-tools/latest/bin/sdkmanager --update
	yes | /opt/android-sdk/cmdline-tools/latest/bin/sdkmanager --licenses
	#AVD
	cd /opt/android-sdk/cmdline-tools/latest/bin
	./sdkmanager "platforms;android-28"
	./sdkmanager "build-tools;28.0.3"
	./sdkmanager "system-images;android-28;default;x86_64"
	avdmanager create avd --name android28 --package "system-images;android-28;default;x86_64"
	##/opt/android-sdk/emulator$ emulator -avd android28



#agregar a ~/.conf/openbox/rc.xml
#LUEGO openbox --reconfigure
<!-- Keybindings for window tiling -->
<keybind key="Super-Left">
  <action name="UnmaximizeFull"/>
  <action name="MoveResizeTo">
    <x>0</x><y>0</y>
    <height>100%</height>
    <width>50%</width>
  </action>
</keybind>
<keybind key="Super-Right">
  <action name="UnmaximizeFull"/>
  <action name="MoveResizeTo">
    <x>-0</x><y>0</y>
    <height>100%</height>
    <width>50%</width>
  </action>
</keybind>
<keybind key="Super-Up">
  <action name="MaximizeFull"/>
</keybind>
