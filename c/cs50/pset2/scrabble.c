#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <cs50.h>

//letras y puntaje
const int abcPuntos[] = {1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10};
const char abcLetras[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
int abcTamano = sizeof(abcPuntos) / sizeof(abcPuntos[0]);

int calcularPuntaje(string palabra);

int main(void)
{
    string resultado = "Tie!";

    // Recibir entradas
    string palabra1 = get_string("Jugador 1: ");
    string palabra2 = get_string("Jugador 2: ");

    // Calcular puntajes
    int puntaje1 = calcularPuntaje(palabra1);
    int puntaje2 = calcularPuntaje(palabra2);

    // Devolver ganador
    if (puntaje1 > puntaje2)
    {
        resultado = "Player 1 wins!";
    }
    else if (puntaje1 < puntaje2)
    {
        resultado = "Player 2 wins!";
    }

    printf("%s\n", resultado);
}

int calcularPuntaje(string palabra)
{
    int puntaje = 0;
    int palabraTamano = strlen(palabra);

    //Convertir a mayusculas
    for (int i = 0; i < palabraTamano; i++)
    {
        palabra[i] = tolower(palabra[i]);
    }

    //puntos
    for (int i = 0; i < palabraTamano; i++)
    {
        int condicion = 0;
        int j = 0;
        do
        {
            if (palabra[i] == abcLetras[j])
            {
                puntaje += abcPuntos[j];
                condicion = 1;
            }
            j++;
        }
        while (condicion == 0 && j < abcTamano);
    }

    return puntaje;
}
