#include "helpers.h"
#include <math.h>

int blurColor(int i, int j, int height, int width, RGBTRIPLE image[height][width], int color);

// Convert image to grayscale
void grayscale(int height, int width, RGBTRIPLE image[height][width])
{
    float gris;

    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            gris = round((image[i][j].rgbtBlue + image[i][j].rgbtGreen + image[i][j].rgbtRed) / 3.00);
            image[i][j].rgbtBlue = gris;
            image[i][j].rgbtGreen = gris;
            image[i][j].rgbtRed = gris;
        }
    }
    return;
}

// Reflect image horizontally
void reflect(int height, int width, RGBTRIPLE image[height][width])
{
    int temp[3];

    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width / 2; j++)
        {
            temp[0] = image[i][j].rgbtBlue;
            temp[1] = image[i][j].rgbtGreen;
            temp[2] = image[i][j].rgbtRed;

            image[i][j].rgbtBlue = image[i][width - j - 1].rgbtBlue;
            image[i][j].rgbtGreen = image[i][width - j - 1].rgbtGreen;
            image[i][j].rgbtRed = image[i][width - j - 1].rgbtRed;

            image[i][width - j - 1].rgbtBlue = temp[0];
            image[i][width - j - 1].rgbtGreen = temp[1];
            image[i][width - j - 1].rgbtRed = temp[2];
        }
    }
    return;
}

// Detect edges
void edges(int height, int width, RGBTRIPLE image[height][width])
{
    RGBTRIPLE temp[height][width];

    int x[3][3] = {{-1, 0, 1}, {-2, 0, 2}, {-1, 0, 1}};
    int y[3][3] = {{-1, -2, -1}, {0, 0, 0}, {1, 2, 1}};

    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            int xRed = 0;
            int xGreen = 0;
            int xBlue = 0;
            int yRed = 0;
            int yGreen = 0;
            int yBlue = 0;

            for (int k = -1; k < 2; k++)
            {
                for (int l = -1; l < 2; l++)
                {
                    if (i + k < 0 || i + k > height - 1)
                    {
                        continue;
                    }
                    if (j + l < 0 || j + l > width - 1)
                    {
                        continue;
                    }

                    xRed += image[i + k][j + l].rgbtRed * x[k + 1][l + 1];
                    xGreen += image[i + k][j + l].rgbtGreen * x[k + 1][l + 1];
                    xBlue += image[i + k][j + l].rgbtBlue * x[k + 1][l + 1];
                    yRed += image[i + k][j + l].rgbtRed * y[k + 1][l + 1];
                    yGreen += image[i + k][j + l].rgbtGreen * y[k + 1][l + 1];
                    yBlue += image[i + k][j + l].rgbtBlue * y[k + 1][l + 1];
                }
            }

            int red = round(sqrt(xRed * xRed + yRed * yRed));
            int green = round(sqrt(xGreen * xGreen + yGreen * yGreen));
            int blue = round(sqrt(xBlue * xBlue + yBlue * yBlue));

            temp[i][j].rgbtRed = (red > 255) ? 255 : red;
            temp[i][j].rgbtGreen = (green > 255) ? 255 : green;
            temp[i][j].rgbtBlue = (blue > 255) ? 255 : blue;
        }
    }

    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            image[i][j].rgbtBlue = temp[i][j].rgbtBlue;
            image[i][j].rgbtGreen = temp[i][j].rgbtGreen;
            image[i][j].rgbtRed = temp[i][j].rgbtRed;
        }
    }

    return;
}

// Blur image
void blur(int height, int width, RGBTRIPLE image[height][width])
{
    RGBTRIPLE temp[height][width];

    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            temp[i][j] = image[i][j];
        }
    }

    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            image[i][j].rgbtRed = blurColor(i, j, height, width, temp, 0);
            image[i][j].rgbtGreen = blurColor(i, j, height, width, temp, 1);
            image[i][j].rgbtBlue = blurColor(i, j, height, width, temp, 2);
        }
    }

    return;
}

int blurColor(int i, int j, int height, int width, RGBTRIPLE imagen[height][width], int color)
{
    float cantidad = 0;
    int suma = 0;

    for (int k = i - 1; k < (i + 2); k++)
    {
        for (int l = j - 1; l < (j + 2); l ++)
        {
            int vale = 1;

            if (k < 0 || k >= height || l < 0 || l >= width)
            {
                vale = 0;
            }

            if (vale == 1)
            {
                if (color == 0)
                {
                    suma += imagen[k][l].rgbtRed;
                }
                else if (color == 1)
                {
                    suma += imagen[k][l].rgbtGreen;
                }
                else if (color == 2)
                {
                    suma += imagen[k][l].rgbtBlue;
                }
                cantidad++;
            }
        }
    }

    return round(suma / cantidad);
}