#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Each person has two parents and two alleles
typedef struct person
{
    char alleles[2];
    struct person *parents[2];
}
person;

const int GENERATIONS = 3;
const int INDENT_LENGTH = 4;

person *create_family(int generations);
void print_family(person *p, int generation);
void free_family(person *p);
char random_allele();

int main(void)
{
    // Seed random number generator
    srand(time(0));

    // Create a new family with three generations
    person *p = create_family(GENERATIONS);

    // Print family tree of blood types
    print_family(p, 0);

    // Free memory
    free_family(p);
}

// Create a new individual with `generations`
person *create_family(int generations)
{
    person *retorno = NULL;

    person *persona = malloc(sizeof(person));

    if (persona != NULL)
    {
        if (generations > 1)
        {
            persona->parents[0] = create_family(generations - 1);
            persona->parents[1] = create_family(generations - 1);
            persona->alleles[0] = persona->parents[0]->alleles[rand() % 2];;
            persona->alleles[1] = persona->parents[1]->alleles[rand() % 2];;
        }

        else
        {
            persona->parents[0] = NULL;
            persona->parents[1] = NULL;
            persona->alleles[0] = random_allele();
            persona->alleles[1] = random_allele();
        }

        retorno = persona;
    }

    return retorno;
}

// Free `p` and all ancestors of `p`.
void free_family(person *p)
{
    if (p != NULL)
    {
        free_family(p->parents[0]);
        free_family(p->parents[1]);
        free(p);
    }
}

// Print each family member and their alleles.
void print_family(person *p, int generation)
{
    // Handle base case
    if (p == NULL)
    {
        return;
    }

    // Print indentation
    for (int i = 0; i < generation * INDENT_LENGTH; i++)
    {
        printf(" ");
    }

    // Print person
    printf("Generation %i, blood type %c%c\n", generation, p->alleles[0], p->alleles[1]);
    print_family(p->parents[0], generation + 1);
    print_family(p->parents[1], generation + 1);
}

// Randomly chooses a blood type allele.
char random_allele()
{
    int r = rand() % 3;
    if (r == 0)
    {
        return 'A';
    }
    else if (r == 1)
    {
        return 'B';
    }
    else
    {
        return 'O';
    }
}
