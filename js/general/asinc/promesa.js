'use strict'

const datos = [
	{titulo: 'dato 1', contenido: 'contenido 1'},
	{titulo: 'dato 2', contenido: 'contenido 2'}
]

function llamada(){
	setTimeout(obtenerDatos, 1000);
}

function obtenerDatos(){
	let salida = '';
	datos.forEach(function(dato){
		salida = salida + dato.titulo;
	})
	console.log(salida);
}

llamada();