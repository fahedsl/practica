#include <stdio.h>
#include <cs50.h>

void formarPiramide(int height);

int main(void)
{
    int altura;

    do
    {
        altura = get_int("Altura: ");
    }
    while (altura < 1 || altura > 8);

    formarPiramide(altura);
}


void formarPiramide(int height)
{
    for (int i = 0; i < height; i++)
    {
        for (int j = 1; j < height - i; j++)
        {
            printf(" ");
        }

        for (int k = 0; k <= i; k++)
        {
            printf("#");
        }

        printf("  ");

        for (int k = 0; k <= i; k++)
        {
            printf("#");
        }

        printf("\n");
    }
}