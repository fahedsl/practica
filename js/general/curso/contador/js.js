window.onload = function(){
	let Contador = new contador();
};

function contador(){
	let total;

	let inicio = function(){
		let uitotal = document.querySelector('h1');
		let aumentar = document.querySelector('.aumentar');
		let guardar = document.querySelector('.guardar');
	
		this.total = 0;

		uitotal.textContent = this.total;
	
		aumentar.addEventListener(
			'click',
			function(){fAumentar(uitotal)}
		);
	
		guardar.addEventListener(
			'click',
			function(){fGuardar(uitotal)}
		);
	}

	let fAumentar = function(numero){
		let cantidad = 1 * numero.textContent;
		numero.textContent = 1 + cantidad;
	}
	
	let fGuardar = function(numero){
		let cantidad = 1 * numero.textContent;
		this.total = this.total + cantidad;
		numero.textContent = 0;
		console.clear();
		console.log('Total:', this.total);
		}

	inicio();
}