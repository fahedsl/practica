#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <cs50.h>

string cifrar(string entrada, int entradaTamano);
int claveTamano = 26;
char clave[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
char abc[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

int main(int argc, string argv[])
{
    int argumentoValido = 0;
    int argumentoUsado = 1;
    int retorno = 0;

    //argumento
    if (argc == argumentoUsado + 1 && strlen(argv[argumentoUsado]) == 26)
    {
        int argumentoValidoTemp = 1;

        for (int i = 0; i < claveTamano; i++)
        {
            int repeticiones = 0;

            if (argumentoValidoTemp == 1)
            {
                if (!isalpha(argv[argumentoUsado][i]))
                {
                    argumentoValidoTemp = 0;
                    retorno = 1;
                    break;
                }
                for (int j = 0; j < claveTamano; j++)
                {
                    if (argv[argumentoUsado][i] == argv[argumentoUsado][j])
                    {
                        repeticiones++;
                        if (repeticiones > 1)
                        {
                            argumentoValidoTemp = 0;
                            retorno = 1;
                            break;
                        }
                    }
                }
            }
            else
            {
                break;
            }
        }

        if (argumentoValidoTemp == 1)
        {
            argumentoValido = 1;
            for (int i = 0; i < claveTamano; i++)
            {
                clave[i] = tolower(argv[argumentoUsado][i]);
            }
        }
    }
    else
    {
        if (argc != argumentoUsado + 1)
        {
            printf("Usage: ./substitution key\n");
            retorno = 1;
        }
        else if (strlen(argv[argumentoUsado]) != 26)
        {
            printf("Key must contain 26 characters.\n");
            retorno = 1;
        }
    }

    if (argumentoValido == 1)
    {
        //leer entrada
        string entrada = get_string("plaintext:  ");
        int entradaTamano = strlen(entrada);

        //cifrar
        string cifrado = cifrar(entrada, entradaTamano);

        //devolver el texto cifrado
        printf("ciphertext: %s\n", cifrado);
    }

    return retorno;
}


string cifrar(string entrada, int entradaTamano)
{
    string respuesta = entrada;

    for (int i = 0; i < entradaTamano; i++)
    {
        if (isalpha(respuesta[i]))
        {
            int esMayuscula = 0;
            if (isupper(respuesta[i]))
            {
                esMayuscula = 1;
            }

            respuesta[i] = tolower((respuesta[i]));
            for (int j = 0; j < claveTamano; j++)
            {
                if (respuesta[i] == abc[j])
                {
                    respuesta[i] = clave[j];
                    break;
                }
            }

            if (esMayuscula == 1)
            {
                respuesta[i] = toupper(respuesta[i]);
            }
        }
    }

    return respuesta;
}