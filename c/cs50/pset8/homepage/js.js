function mostrar(elementoLeidoId, elementoMostradoId){
	let leido = document.getElementById(elementoLeidoId);
	let mostrado = document.getElementById(elementoMostradoId);
	mostrado.textContent = leido.value;
}

function pantallaTamano(ancho, alto){
	let anchoMostrar = document.getElementById(ancho);
	let altoMostrar = document.getElementById(alto);
	anchoMostrar.textContent = window.screen.availWidth;
	altoMostrar.textContent = window.screen.availHeight;
};