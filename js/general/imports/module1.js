class mateA{
	constructor(){
		console.log('mateA creado');
	}

	f1(){
		return "mateA.f1";
	}

	f2(){
		return "mateA.f2";
	}
}

class mateB{
	constructor(){
		console.log('mateB creado');
	}

	f1(){
		return "mateB.f1";
	}

	f2(){
		return "mateB.f2";
	}
}

module.exports.mateA = mateA;
module.exports.mateB = mateB;
