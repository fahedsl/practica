#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

// Number of bytes in .wav header
const int HEADER_SIZE = 44;

int main(int argc, char *argv[])
{
    // Check command-line arguments
    if (argc != 4)
    {
        printf("Usage: ./volume input.wav output.wav factor\n");
        return 1;
    }

    // Open files and determine scaling factor
    FILE *input = fopen(argv[1], "r");
    if (input == NULL)
    {
        printf("Could not open file.\n");
        return 1;
    }

    FILE *output = fopen(argv[2], "w");
    if (output == NULL)
    {
        printf("Could not open file.\n");
        return 1;
    }

    float factor = atof(argv[3]);

    int MAX = 32766;
    uint8_t b8[HEADER_SIZE];
    int16_t b16;
    int16_t *pb16 = &b16;

    // TODO: Copy header from input file to output file
    fread(b8, sizeof(uint8_t), HEADER_SIZE, input);
    fwrite(b8, sizeof(uint8_t), HEADER_SIZE, output);

    // TODO: Read samples from input file and write updated data to output file
    while (fread(pb16, sizeof(int16_t), 1, input) > 0)
    {
        b16 = b16 * factor;
        if (b16 >= MAX)
        {
            b16 = MAX;
        }
        fwrite(pb16, sizeof(int16_t), 1, output);
    }

    // Close files
    fclose(input);
    fclose(output);
}
