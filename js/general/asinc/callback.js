'use strict'

// Este video es explica el uso del event loop, asincronía y callbacks:
// VIDEO: https://www.youtube.com/watch?v=8aGhZQkoFbQ
// HERRAMIENTA: http://latentflip.com/loupe

// Datos usados
let datos = [
	titulo: 'dato 1',
	titulo: 'dato 2'
];
let dato = titulo: 'dato 3';

// Funcion con callbacks: que tenga callback no afecta a que sea asíncrono, que use setTimeout sí.
function llamada(paso1, paso2){
	paso1();
	setTimeout(paso2, 0);
}

// Funcion simple Síncrona
function obtenerDatos(){
	datos.forEach(function(dato){
		console.log(dato.titulo);
	})
}

// Funcion simple Asíncrona
function insertarDatos(){
	setTimeout(function(){
		datos.push(dato);
		console.log('insertado');
	}, 1000);
}

// Llamada a la funcion inicial
llamada(insertarDatos, obtenerDatos);