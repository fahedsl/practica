'use strict'

const datos = [
	{titulo: 'dato 1', contenido: 'contenido 1'},
	{titulo: 'dato 2', contenido: 'contenido 2'}
]


function llamada(paso1, paso2){
	setTimeout(paso1, 1000);
	paso2();
}

function obtenerDatos(){
	let salida = '';
	datos.forEach(function(dato){
		salida = salida + dato.titulo;
	})
	console.log(salida);
}

function insertarDatos(){
	let dato = {titulo: 'dato 3', contenido: 'contenido 3'};
	datos.push(dato);
}

llamada(insertarDatos, obtenerDatos);