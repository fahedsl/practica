--In 7.sql, write a SQL query that returns the average energy of songs that are by Drake.
--Your query should output a table with a single column and a single row containing the average energy.
--You should not make any assumptions about what Drake’s artist_id is.

select avg(s.energy)
from songs as s
join artists as a
	on a.id = s.artist_id
where
	a.name = 'Drake';
