#!/bin/bash

read -p "Ingresar usuario: " usuario
usuario="${usuario:-${USER}}"
grupo=$(id -gn $usuario)

###GENERAL###

#BORRAR
	apt purge -y catfish
	apt purge -y mousepad
	apt purge -y xfce4-notes
	apt purge -y xfce4-notes-plugin
	apt purge -y xfce4-weather-plugin
	apt purge -y xfce4-places-plugin
	apt purge -y sgt-launcher
	apt purge -y sgt-puzzles
	apt purge -y gnome-mines
	apt purge -y gnome-sudoku
	apt purge -y pidgin
	apt purge -y anydesk
	apt purge -y parole
	apt purge -y firefox
	apt purge -y libreoffice
	apt purge -y xfce4-terminal
	apt purge -y xterm
	apt purge -y stterm
	apt purge -y xfce4-screenshooter
	apt purge -y compton
	apt purge -y geany*
	apt purge -y abiword*
	apt purge -y gnumeric*
	apt-get purge -y blue*
	rm -r /home/$usuario/Documents
	rm -r /home/$usuario/Music
	rm -r /home/$usuario/Pictures
	rm -r /home/$usuario/Public
	rm -r /home/$usuario/Templates
	rm -r /home/$usuario/Videos
	rm -r /home/$usuario/dDocuments
	rm -r /home/$usuario/music
	rm -r /home/$usuario/pictures
	rm -r /home/$usuario/public
	rm -r /home/$usuario/templates
	rm -r /home/$usuario/videos

	apt update
	apt upgrade -y
	apt install -y git
	apt install -y snap
	apt install -y snapd
	apt install -y wget
	apt install -y curl
	apt install -y zip
	apt install -y unzip
	apt install -y rar
	apt install -y unrar
	apt install -y inkscape
	apt install -y gimp
	apt install -y vlc
	apt install -y firefox
	apt install -y chromium-browser
	apt install -y libreoffice
	apt install -y ffmpeg
	apt install -y libavcodec-extra-53
	apt install -y obs-studio
	apt install -y kdenlive
	apt install -y flameshot
	apt install -y transmission
	apt install -y neomutt
	apt install -y audacity
	apt install -y fbreader
	apt install -y xbindkeys
	#terminal
	snap install alacritty --classic
	#spotify
	curl -sS https://download.spotify.com/debian/pubkey_0D811D58.gpg | apt-key add -
	echo "deb http://repository.spotify.com stable non-free" | tee /etc/apt/sources.list.d/spotify.list
	apt-get update
	apt-get install -y spotify-client
	#gammy
	apt install -y build-essential
	apt install -y libgl1-mesa-dev
	apt install -y libxext-dev
	apt install -y libxxf86vm-dev
	apt install -y qtbase5-dev
	apt install -y qtchooser
	apt install -y qt5-qmake
	apt install -y qtbase5-dev-tools
	apt install -y qt5-default
	git clone https://github.com/Fushko/gammy.git
	mv gammy /opt/gammy
	cd /opt/gammy
	qmake Gammy.pro
	make install
#chats
	snap install core
	#telegram
	snap install telegram-desktop
	#whatsapp
	snap install whatsdesk
	#zoom
	wget https://zoom.us/client/latest/zoom_amd64.deb -O /tmp/zoom.deb
	apt install -y /tmp/zoom.deb
	rm -rf /tmp/zoom.deb
#remoto
	#teamviewer
	wget https://download.teamviewer.com/download/linux/teamviewer_amd64.deb -O /tmp/teamviewer.deb
	apt install -y /tmp/teamviewer.deb
	rm -rf /tmp/teamviewer.deb
	#anydesk
	wget -qO - https://keys.anydesk.com/repos/DEB-GPG-KEY | sudo apt-key add -
	echo "deb http://deb.anydesk.com/ all main" | sudo tee /etc/apt/sources.list.d/anydesk-stable.list
	apt update
	apt install -y anydesk
	systemctl disable anydesk.service
	#graficos
	deb http://deb.debian.org/debian buster-backports main contrib non-free
	apt update
	apt install -y nvidia-driver
	apt install -y firmware-linux
	apt install -y firmware-misc-nonfree
	#glxinfo|egrep "OpenGL vendor|OpenGL renderer"

#ARCHIVOS (CREAR, REEMPLAZAR)
	#gammy - inicio
	cat > /home/$usuario/.config/autostart/gammy.desktop <<EOL
[Desktop Entry]
Type=Application
Exec=/opt/gammy/gammy
Hidden=false
NoDisplay=false
X-GNOME-Autostart-enabled=true
Name=Gammy adaptive screen brightness
EOL
	#nitrogen - inicio
	cat > /home/$usuario/.config/autostart/nitrogen.desktop <<EOL
[Desktop Entry]
Type=Application
Exec=/usr/bin/nitrogen
Hidden=false
NoDisplay=false
X-GNOME-Autostart-enabled=true
Name=Nitrogen
EOL
	#Openbox - Atajos de teclado 



### PROGRAMACION ###

#"LENGUAJES y ENTORNOS"
#apache
	apt install -y apache2
	apt install -y libapache2-mod-fcgid
	apt install -y libapache2-mod-php
	apt install -y software-properties-common
	a2enmod actions fcgid alias proxy_fcgi rewrite
#php
	apt install -y php
	apt install -y php-cli
	apt install -y php-curl
	apt install -y php-fpm
	apt install -y php-gd
	apt install -y php-json
	apt install -y php-mbstring
	apt install -y php-mysql
	apt install -y php-xml
	apt install -y php-mysql
	apt install -y php-mysqlnd-ms
	apt install -y php-pear
	phpenmod mysqli
	service apache2 restart
#C++
	apt install -y build-essential
#Rust
	curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
	source $HOME/.cargo/env
	rustc --version
#node.js
	apt install -y nodejs
	apt install -y npm
#java
	apt install -y openjdk-11-jdk
#android
	apt install -y adb
	apt install -y fastboot
	apt-key adv --keyserver keyserver.ubuntu.com --recv-keys D7CC6F019D06AF36
	add-apt-repository -y ppa:cwchien/gradle
	apt-get install gradle -y
	gradle -v
#mysql
	apt install -y mariadb-server
	apt install -y mariadb-client
	mysql -Bse "CREATE USER 'f'@'localhost' IDENTIFIED BY '12';"
	mysql -Bse "GRANT ALL PRIVILEGES ON * . * TO 'f'@'localhost';"
	mysql -Bse "flush privileges;"
#couchdb
	apt install -y curl
	apt install -y build-essential
	apt install -y erlang
	apt install -y libicu-dev
	apt install -y libmozjs-dev
	apt install -y libcurl4-openssl-dev
#couchbase

#EDITORES
#sublime
	wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | apt-key add -
	echo "deb https://download.sublimetext.com/ apt/stable/" | tee /etc/apt/sources.list.d/sublime-text.list
	apt update -y
	apt install -y sublime-text
#Intellij Idea
	apt update && sudo apt install -y snapd
	snap install intellij-idea-community --classic

#HERRAMIENTAS
#dbeaver
	wget https://dbeaver.io/files/dbeaver-ce_latest_amd64.deb -O /tmp/dbeaver.deb
	apt install -y /tmp/dbeaver.deb
	rm /tmp/dbeaver.deb
#filezilla
	apt install -y filezilla
#httpie (apis)
	apt install -y httpie
#sourcetrail
	wget https://github.com/CoatiSoftware/Sourcetrail/releases/download/2021.1.30/Sourcetrail_2021_1_30_Linux_64bit.tar.gz -O /home/$USER/sourcetrail.tar.gz
	tar -xzvf sourcetrail.tar.gz -C /home/$USER
	rm -r /home/$USER/sourcetrail.tar.gz
	cd /home/$USER/Sourcetrail
	./install.sh
	cd rm -R /home/$USER/Sourcetrail

#sdk
	wget https://dl.google.com/android/repository/commandlinetools-linux-6858069_latest.zip -O /tmp/android-sdk.zip
	mkdir -p /opt/android-sdk
	unzip /tmp/android-sdk.zip -d /opt/android-sdk
	rm -r /tmp/android-sdk.zip
	echo 'export ANDROID_SDK_ROOT="/opt/android-sdk"' > /etc/profile.d/android.sh
	echo 'export ANDROID_HOME="$ANDROID_SDK_ROOT"' >> /etc/profile.d/android.sh
	echo 'export PATH="$PATH:$ANDROID_SDK_ROOT/cmdline-tools/latest/bin"' >> /etc/profile.d/android.sh
	echo 'export PATH="$PATH:$ANDROID_SDK_ROOT/cmdline-tools/tools/bin"' >> /etc/profile.d/android.sh
	echo 'export PATH="$PATH:$ANDROID_SDK_ROOT/platform-tools"' >> /etc/profile.d/android.sh
	echo 'export PATH="$PATH:$ANDROID_SDK_ROOT/tools"' >> /etc/profile.d/android.sh
	export ANDROID_SDK_ROOT="/opt/android-sdk"
	cd /etc/profile.d/
	chmod +x android.sh
	./android.sh
	chmod -x android.sh
	#correccion ubicacion tools
	cd $ANDROID_SDK_ROOT
	cp -r $ANDROID_SDK_ROOT/cmdline-tools $ANDROID_SDK_ROOT/cmdline-tools2
	rm -r $ANDROID_SDK_ROOT/cmdline-tools/*
	mv $ANDROID_SDK_ROOT/cmdline-tools2 $ANDROID_SDK_ROOT/cmdline-tools/tools
	cd $ANDROID_SDK_ROOT/cmdline-tools/tools/bin
	./sdkmanager
	./sdkmanager platform-tools
	./sdkmanager "platforms;android-30"
	./sdkmanager "build-tools;30.0.3"
	./sdkmanager "cmdline-tools;latest"
	yes | ./sdkmanager --update
	yes | ./sdkmanager --licenses
	#conseguir herramienta "android"
