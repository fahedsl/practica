import string

texto = input('Ingresa el texto:')
letras = 0
palabras = 1
oraciones = 0

if (texto != ''):
    for i in range(len(texto)):
        if (texto[i].isalpha()):
            letras += 1
        elif (texto[i] == ' ' and texto[i + 1]):
            palabras += 1
        elif (texto[i] == '?' or texto[i] == '!' or texto[i] == '.'):
            oraciones += 1

    L = float(letras / palabras * 100)
    S = float(oraciones / palabras * 100)
    luhn = round((float)(0.0588 * L - 0.296 * S - 15.8))

    if (luhn >= 16):
        print('Grade 16+')
    elif (luhn < 1):
        print('Before Grade 1')
    else:
        print('Grade ', luhn)