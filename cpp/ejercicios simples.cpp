//https://adriann.github.io/programming_problems.html
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <vector>

using namespace std;

//---------------------------------------------------------------------------

int holaMundo(){
	cout << "Hola, mundo" << endl;
	return (0);
}

//---------------------------------------------------------------------------

int nombreYSaludo(){
	string nombre;
	cout << "¿Cómo te llamas?" << endl;
	cin >> nombre;
	cout << "Hola, " << nombre << endl;
	return (0);
}

//---------------------------------------------------------------------------

int nombreYSaludo2(){
	string nombre;
	cout << "¿Cómo te llamas?" << endl;
	cin >> nombre;
	if (nombre == "Ada" || nombre == "Bob")
	{
		cout << "Hola, " << nombre << endl;
	}
	else{
		cout << "No te conosco, aléjate de mí." << endl;
	}
	return (0);
}

//---------------------------------------------------------------------------

int factorialN(){
	int numero, factorial = 0;
	cout << "Número: ";
	cin >> numero;
	if (numero == 0){
		cout << "Factorial: " << 0 << endl;
	}
	else if (numero > 0){
		for (int i = numero; i > 0; i--){
			factorial = factorial + i;
		}
		cout << "Factorial: " << factorial << endl;
	}
	else{
		for (int i = numero; i < 0; i++){
			factorial = factorial + i;
		}
		cout << "Factorial : " << factorial << endl;
	}
	return (0);
}

//---------------------------------------------------------------------------

int sumaMult3Y5(){
	int numero, suma = 0;
	cout << "Número: ";
	cin >> numero;
	if (numero > 0){
		for (int i = numero; i > 0; i--){
			if ( i % 3 == 0 || i % 5 == 0){
				suma = suma + i;
			}
		}
	}
	else{
		for (int i = numero; i < 0; i++){
			if ( i % 3 == 0 || i % 5 == 0){
				suma = suma + i;
			}
		}
	}
	cout << "Suma: " << suma << endl;
	return (0);
}

//---------------------------------------------------------------------------

int multiplicacion1HastaN(){
	int numero, resultado;
	char operacion = 's';
	bool signo = 0;

	cout << "Número: ";
	cin >> numero;
	cout << "Selecciona Suma o Multiplicacion (S/m): ";
	cin >> operacion;
	
	if (numero == 0){
		cout << 0 << endl;
	}
	else {
		if (numero < 0){
			signo = 1;
			numero *= -1;
			cout << "num = " << numero << endl;
		}

		if (operacion == 's' or operacion == 'S'){
			resultado = 0;
			for (int i = 1; i <= numero; i++){
				resultado = resultado + i;
			}
			if (signo == 1){
				resultado *= -1;
			}
		}

		if (operacion == 'm' or operacion == 'M'){
			resultado = 1;
			for (int i = 1; i <= numero; i++){
				resultado = resultado * i;
			}
			if (signo == 1 && numero % 2 != 0){
				resultado *= -1;
			}
		}
		cout << "resultado: " << resultado << endl;
	}

	return (0);
}

//---------------------------------------------------------------------------

int tablasDeMultiplicar(){
	int max = 12;
	for (int i = 1; i <= max; i++){
		cout << "\t" << i;
	}
	cout << endl;
	for (int i = 1; i <= max; i++){
		cout << i;
		for (int j = 1; j <= max; j++){
			cout << "\t" << i * j;
		}
		cout << endl;
	}
	return (0);
}

//---------------------------------------------------------------------------

int primosHastaN(){
	int numero;
	bool prueba;

	cout << "Número máximo: ";
	cin >> numero;

	if (numero >= 1){
		cout << 1 << "\t";
		if (numero >= 2){
			cout << 2 << "\t";
		}
		if (numero >= 3){
			for (int i = 3; i <= numero; i++){
				prueba = 0;
				for(int j = 2; j < i; j++){
					if (i % j == 0){
						prueba = 1;
						break;
					}
				}
				if (prueba != 1){
					cout << i << "\t";
				}
			}
		}
	}
	else {
		cout << "pos nada.";
	}

	cout << endl;

	return (0);
}

//---------------------------------------------------------------------------

int juegoDeAdivinar(){
	int numero = 0, objetivo = 0, conteo = 1;
	bool verif;

	srand(time(NULL));
	objetivo = rand() % 100 +1;

	while(conteo <= 10 && conteo != objetivo){
		cout << "Intento " << conteo << ": ";
		cin >> numero;

		if (numero == objetivo){
			cout << "Correcto."<< endl;
			break;
		}
		else{
			if (numero < objetivo){
				cout << numero << " es menor que el número buscado."<< endl;
			}
			else if (numero > objetivo){
				cout << numero << " es mayor que el número buscado."<< endl;
			}
			conteo++;
		}
	}
	if (numero != objetivo){
		cout << "El número correcto es " << objetivo << "." << endl;
	}
	return (0);
}

//---------------------------------------------------------------------------

int anosBisiestos(){
	int numero, conteo = 0;
	bool verif;

	cout << "Año inicial: ";
	cin >> numero;

	cout << "Bisiestos:\t";
	for (int i = numero; i < numero + 16; i++){
		verif = 0;
		if (i % 4 == 0){
			if (i % 100 == 0){
				if (i % 400 == 0){
					verif = 1;
				}
			}
			else{
				verif = 1;
			}
		}
		if (verif == 1){
			cout << i << "\t";
		}
	}
	cout << endl;
	return (0);
}

//---------------------------------------------------------------------------

int eleccionOpcionesPrograma(string *opciones, int tam1){
	int cont1 = 1;
	int eleccion = -1;

	cout << "ELIGE UNA OPCION"<< endl << endl;
	
	for (int i = 1; i < tam1+1; ++i)
		cout << cont1++ << ": " << opciones[i] << endl;

	cout << endl;
	cout << "Ingresa tu elección (1/"<< tam1 << ") o 0 para terminar: ";
	cin >> ws >> eleccion;
	cin.ignore();
	return (eleccion);
}

//---------------------------------------------------------------------------

int main(){
	int tam1 = 10;
	string opciones[tam1 + 1];
	
	opciones[1] = "Hello World.";
	opciones[2] = "Nombre y saludo.";
	opciones[3] = "Nombre y saludo solo a Ada o Bob.";
	opciones[4] = "Pide numero N, factorial de N.";
	opciones[5] = "Pide numero N, sumatoria de multiplos de 3 y 5 hasta N.";
	opciones[6] = "Pide numero N, suma o multiplicacion de 1 hasta N";
	opciones[7] = "Tablas de multiplicar hasta el 12.";
	opciones[8] = "Pide numero N, Numeros primos hasta N.";
	opciones[9] = "Adivina el numero, 10 oportunidades, indica si mayor o menor";
	opciones[10] = "Pide año, muestra los 20 siguientes años bisiestos";

	int eleccion = eleccionOpcionesPrograma(opciones, tam1);
	
	if (eleccion <= 0){
		return(0);
	}
	else {
		cout  << endl << "* " << opciones[eleccion] << endl;

		switch(eleccion){
			case 1:
				holaMundo();
				break;
			case 2:
				nombreYSaludo();
				break;
			case 3:
				nombreYSaludo2();
				break;
			case 4:
				factorialN();
				break;
			case 5:
				sumaMult3Y5();
				break;
			case 6:
				multiplicacion1HastaN();
				break;
			case 7:
				tablasDeMultiplicar();
				break;
			case 8:
				primosHastaN();
				break;
			case 9:
				juegoDeAdivinar();
				break;
			case 10:
				anosBisiestos();
				break;
			default:
				break;
		}
	} 
}
