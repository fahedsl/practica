# Find It Quickly


## Video Demo: https://youtu.be/etIsGT6Zo9U


## Description:
	This project, "Find It Quickly", is a website which purpose is to be a simple way to find specific kinds of venues and services around your location.
	It has been made using Python with Flask, HTML, CSS, JS. It used browser's Geolocation API and consumes Google Maps APIs from the client side.
	It has a simple interface and is very easy to navigate.
	It has been uploaded to Heroku where it is currently available on:
##	https://find-it-q.herokuapp.com


## Project Files

# app.py
	This is the main "logic" file in the app. Here is decided which of the 2 screens to use based on the parameters passed (type).

# static/css/style.css
	This is the file containing the personalized styles used on the site, aditional to the ones used on Bootstrap.

# static/js/inicio.js
	Here can be found the code that animates the text in inicio.html and with which the buttons are shaped to squares.

# static/js/mapa.js
	This is the JS where most of the hardwork has gone.
	Here, we get the geolocation from the browser's Geolocation API.
	After that, Google maps APIs are requested for information about the places around the current location and the

# templates/pagina.html
	This is the base template for the website.html

# templates/inicio.html
	This is the template for the landing page, where the options are diplayed. It extends pagina.html

# templates/mapa.html
	This is the template for the map itself. It extends pagina.html

# static/list.json
	This is the content source for the app.
	The buttons in inicio.html and the options in the menu are dynamically populated using the information in this file.

# static/img
	Here are saved the images and icons which are used in the app.

# venv, .gitignore, Procfile, requirements.txt
	These are config files that are necesary to config and make the app work on the server in Heroku.