#!/bin/bash
# Server Configuration
# Created on 12.05.2022
# Version 1.0
PHP=`php -v | head -n1`
echo "=========================="
echo "Packages Configurations"
echo "=========================="
echo "1. Apache Installation \n2. Virtual Host Creation \n3. PHP Installation with Extentions \n4. Mysql-Server \n5. Node & Composer"
echo "=========================="
echo "Enter the option"
echo "=========================="
read USER
if [ $USER = 1 ]; then 
    ######## Apache Installation Setup ######
    echo "=========================="
    echo "Started apache2 installations"
    echo "=========================="
    sudo apt-get install apache2
    echo "=========================="
    echo "Creating services"
    echo "=========================="
    sudo systemctl enable apache2
    systemctl start apache2.service
    echo "=========================="
    echo "Installation has been completed"
    echo "=========================="
    exit;
elif [ $USER = 2 ]; then
    ######## Virtual Host Creations ######
    echo "Which protocol want to configure 1.HTTP or 2.HTTPS?"
    read HTTP
    if [ $HTTP = 1 ]; then
        echo "=========================="
        echo "Enter domain name ?"
        echo "=========================="
        read localdomain
        echo "=========================="
        echo "Set directory path ?"
        echo "=========================="
        read dpath
        mailid="webmaster@localhost"
        sitesEnable='/etc/apache2/sites-enabled/'
        sitesAvailable='/etc/apache2/sites-available/'
        sitesAvailabledomain=$sitesAvailable$localdomain.conf
        echo "Creating a vhost for $sitesAvailabledomain with a webroot $dpath"
        ### create virtual host rules file
        echo "
            <VirtualHost *:80>
            ServerAdmin $mailid
            ServerName $localdomain
            DocumentRoot $dpath
            <Directory $dpath>
                Options Indexes FollowSymLinks MultiViews
            AllowOverride All
            Order allow,deny
            allow from all
            </Directory>
            ErrorLog /var/log/apache2/$localdomain.log
            CustomLog /var/log/apache2/access.log combined
            </VirtualHost>" > $sitesAvailabledomain
        echo "\nNew Virtual Host Created"
        sed -i "1s/^/127.0.0.1 $localdomain\n/" /etc/hosts
        sudo a2ensite $localdomain
        sudo a2enmod rewrite
        sudo service apache2 reload
        echo "=========================="
        echo "Configurations completed to check http://$localdomain"
        echo "=========================="
        exit;
    elif [ $HTTP = 2 ]; then
        echo "=========================="
        echo "Enter domain name ?"
        echo "=========================="
        read localdomain
        echo "=========================="
        echo "Set directory path ?"
        echo "=========================="
        read dpath
        mailid="webmaster@localhost"
        sitesEnable='/etc/apache2/sites-enabled/'
        sitesAvailable='/etc/apache2/sites-available/'
        sitesAvailabledomain=$sitesAvailable$localdomain.conf
        echo "Creating a vhost for $sitesAvailabledomain with a webroot $dpath"
        ### create virtual host rules file
        echo "
            <VirtualHost *:443>
            ServerAdmin $mailid
            ServerName $localdomain
            DocumentRoot $dpath
            <Directory $dpath>
                Options Indexes FollowSymLinks MultiViews
            AllowOverride All
            Order allow,deny
            allow from all
            </Directory>
            SSLEngine on
            SSLCertificateFile /etc/ssl/certs/apache-selfsigned.crt
            SSLCertificateKeyFile /etc/ssl/private/apache-selfsigned.key
            ErrorLog /var/log/apache2/$localdomain.log
            CustomLog /var/log/apache2/access.log combined
            </VirtualHost>" > $sitesAvailabledomain
        echo "\nNew Virtual Host Created"
        sed -i "1s/^/127.0.0.1 $localdomain\n/" /etc/hosts
        sudo a2ensite $localdomain
        sudo a2enmod ssl
        sudo a2enmod rewrite
        sudo service apache2 reload
        echo "=========================="
        echo "Configurations completed to check http://$localdomain"
        echo "=========================="
        #SSL-Creationforlocal
        #sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/apache-selfsigned.key -out /etc/ssl/certs/apache-selfsigned.crt
        exit;
    else 
        echo "Incorrect values, please try again!!"
    fi
elif [ $USER = 3 ]; then  
    ########### PHP Installations #########
    echo "=========================="
    echo "Versions lists { 7.1 ,7.2 ,7.4 ,7.4 ,8.0 8.1 }"
    echo "Which versions want to install ?"
    PHPV1=7.1
    PHPV2=7.2
    PHPV3=7.3
    PHPV4=7.4
    PHPV5=8.0
    PHPV6=8.1
    read PHPVersions
    if [ $PHPVersions = $PHPV1 ]; then
        sudo add-apt-repository ppa:ondrej/php -y
        sudo apt-get update -y
        sudo apt-get install -y php7.1
        sudo apt install php7.1-fpm php7.1-common php7.1-mysql php7.1-xml php7.1-xmlrpc php7.1-curl php7.1-gd php7.1-imagick php7.1-cli php7.1-dev php7.1-imap php7.1-mbstring php7.1-soap php7.1-zip php7.1-bcmath -y
        sudo a2enmod php7.1
        sudo a2dismod php7.2
        sudo a2dismod php7.3
        sudo a2dismod php7.4
        sudo a2dismod php8.0
        sudo a2dismod php8.1
        sudo update-alternatives --set php /usr/bin/php7.1
        sudo systemctl restart apache2
        php -v
        echo "==============================="
        echo "Installations has been completed"
        echo "================================"
    elif [ $PHPVersions = $PHPV2 ]; then
        sudo add-apt-repository ppa:ondrej/php -y
        sudo apt-get update -y
        sudo apt-get install -y php7.2
        sudo apt install php7.2-fpm php7.2-common php7.2-mysql php7.2-xml php7.2-xmlrpc php7.2-curl php7.2-gd php7.2-imagick php7.2-cli php7.2-dev php7.2-imap php7.2-mbstring php7.2-soap php7.2-zip php7.2-bcmath -y
        sudo a2dismod php7.1
        sudo a2enmod php7.2
        sudo a2dismod php7.3
        sudo a2dismod php7.4
        sudo a2dismod php8.0
        sudo a2dismod php8.1
        sudo update-alternatives --set php /usr/bin/php7.2
        sudo systemctl restart apache2
        php -v
        echo "==============================="
        echo "Installations has been completed"
        echo "================================"
    elif [ $PHPVersions = $PHPV3 ]; then
        sudo add-apt-repository ppa:ondrej/php -y
        sudo apt-get update -y
        sudo apt-get install -y php7.3
        sudo apt install php7.3-fpm php7.3-common php7.3-mysql php7.3-xml php7.3-xmlrpc php7.3-curl php7.3-gd php7.3-imagick php7.3-cli php7.3-dev php7.3-imap php7.3-mbstring php7.3-soap php7.3-zip php7.3-bcmath -y
        sudo a2dismod php7.1
        sudo a2dismod php7.2
        sudo a2enmod php7.3
        sudo a2dismod php7.4
        sudo a2dismod php8.0
        sudo a2dismod php8.1
        sudo update-alternatives --set php /usr/bin/php7.3
        sudo systemctl restart apache2
        php -v
        echo "==============================="
        echo "Installations has been completed"
        echo "================================"
    elif [ $PHPVersions = $PHPV4 ]; then
        sudo add-apt-repository ppa:ondrej/php -y
        sudo apt-get update -y
        sudo apt-get install -y php7.4
        sudo apt install php7.4-fpm php7.4-common php7.4-mysql php7.4-xml php7.4-xmlrpc php7.4-curl php7.4-gd php7.4-imagick php7.4-cli php7.4-dev php7.4-imap php7.4-mbstring php7.4-soap php7.4-zip php7.4-bcmath -y
        sudo a2dismod php7.1
        sudo a2dismod php7.2
        sudo a2dismod php7.3
        sudo a2enmod php7.4
        sudo a2dismod php8.0
        sudo a2dismod php8.1
        sudo update-alternatives --set php /usr/bin/php7.4
        sudo systemctl restart apache2
        php -v
        echo "==============================="
        echo "Installations has been completed"
        echo "================================"
    elif [ $PHPVersions = $PHPV5 ]; then
        sudo add-apt-repository ppa:ondrej/php -y
        sudo apt-get update -y
        sudo apt-get install -y php8.0
        sudo apt install php8.0-fpm php8.0-common php8.0-mysql php8.0-xml php8.0-xmlrpc php8.0-curl php8.0-gd php8.0-imagick php8.0-cli php8.0-dev php8.0-imap php8.0-mbstring php8.0-soap php8.0-zip php8.0-bcmath -y
        sudo a2dismod php7.1
        sudo a2dismod php7.2
        sudo a2dismod php7.3
        sudo a2dismod php7.4
        sudo a2enmod php8.0
        sudo a2dismod php8.1
        sudo update-alternatives --set php /usr/bin/php8.0
        sudo systemctl restart apache2
        php -v
        echo "==============================="
        echo "Installations has been completed"
        echo "================================"
    elif [ $PHPVersions = $PHPV6 ]; then
        sudo add-apt-repository ppa:ondrej/php -y
        sudo apt-get update -y
        sudo apt-get install -y php8.1
        sudo apt install php8.1-fpm php8.1-common php8.1-mysql php8.1-xml php8.1-xmlrpc php8.1-curl php8.1-gd php8.1-imagick php8.1-cli php8.1-dev php8.1-imap php8.1-mbstring php8.1-soap php8.1-zip php8.1-bcmath -y
        sudo a2dismod php7.1
        sudo a2dismod php7.2
        sudo a2dismod php7.3
        sudo a2dismod php7.4
        sudo a2dismod php8.0
        sudo a2enmod php8.1
        sudo update-alternatives --set php /usr/bin/php8.1
        sudo systemctl restart apache2
        php -v
        echo "==============================="
        echo "Installations has been completed"
        echo "================================"
    else 
        echo "Incorrect values, please try again!!"
    fi
elif [ $USER = 4 ]; then

    echo "=========================="
    echo "1. Mysql install \n2. Modify mysql"
    read mysql
    if [ $mysql = 1 ]; then
        echo "=========================="
        echo "Started Mysql installations"
        echo "=========================="
        sudo apt install mysql-server -y
        sudo systemctl start mysql.service
    elif [ $mysql = 2 ]; then
        echo "=========================="
        echo "1. Set root password \n2. Create account"
        echo "=========================="
        read mysqlmodify
        if [ $mysqlmodify = 1 ]; then
            echo "=========================="
            echo "Enter old password"
            echo "=========================="
            read oldpassword
             echo "Enter New password"
            echo "=========================="
            read newpassword
            #mysql -u root -p${rootpasswd} -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '$newpassword';"
            mysql -u root -p${oldpassword} -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '$newpassword';"
            sudo systemctl restart mysql.service
            exit;
            echo "Password has been created $newpassword"
            if [ $? = 1 ]; then
                echo "****Please check your password - Already created***"
            fi
        elif [ $mysqlmodify = 2 ]; then
            echo "Please enter root user MySQL password!"
            read rootpasswd
            mysql -uroot -p${rootpasswd} -e exit
                if [ $? = 0 ]; then
                echo "Enter database name!"
                read dbname
                echo "Creating new MySQL database..."
                mysql -uroot -p${rootpasswd} -e "CREATE DATABASE ${dbname} /*\!40100 DEFAULT CHARACTER SET utf8 */;"
                echo "Database successfully created!"
                echo "Enter database user!"
                read username
                echo "Enter the PASSWORD for database user!"
                read  userpass
                echo "Creating new user..."
                mysql -uroot -p${rootpasswd} -e "CREATE USER ${username}@localhost IDENTIFIED BY '${userpass}';"
                echo "User successfully created!"
                echo "Granting ALL privileges on ${dbname} to ${username}!"
                mysql -uroot -p${rootpasswd} -e "GRANT ALL PRIVILEGES ON ${dbname}.* TO '${username}'@'localhost';"
                mysql -uroot -p${rootpasswd} -e "FLUSH PRIVILEGES;"
                echo "Account Created :)"
                echo "================================"
                echo "Username: $username & $userpass"
                echo "================================"
                exit;
                else
                    echo "================================"
                    echo "**Incorrect Password @root **You are not eligible to creat account** :("
                    echo "================================"

            fi
        fi
    fi
elif [ $USER = 5 ]; then
    echo "================================"
    echo "NPM installation going on"
    echo "================================"
    sudo apt-get install nodejs -y
    sudo apt-get install npm -y
    npm -v
    echo "================================"
    echo "Composer installation going on"
    echo "================================"
    sudo apt-get install composer -y
    sudo apt install php-cli unzip -y
    cd ~
    curl -sS https://getcomposer.org/installer -o /tmp/composer-setup.php
    HASH=`curl -sS https://composer.github.io/installer.sig`
    php -r "if (hash_file('SHA384', '/tmp/composer-setup.php') === '$HASH') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
    sudo php /tmp/composer-setup.php --install-dir=/usr/local/bin --filename=composer
    composer
fi
