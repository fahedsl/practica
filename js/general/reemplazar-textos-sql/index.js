let texto = 'select * from texto where name=:nombre and id=:id and x=:bool';

function parametro(nombreParam, valorParam){
	let continuar = true;
	let retorno = false;
	let nombre = null;
	let valor = null;
	
	if(typeof(valorParam) === 'string'){
		nombre = ':' + nombreParam;
		valor = '\'' + valorParam + '\'';
	}
	else if(typeof(valorParam) === 'number'
	|| typeof(valorParam) === 'boolean'){
		nombre = ':' + nombreParam;
		valor = valorParam;
	}
	else{
		continuar = false;
	}

	if(continuar){
		if(texto.search(nombre) != -1){
			texto = texto.replace(nombre, valor);
			retorno = true;
		}
	}

	return retorno;
}

parametro('nombre', 'juan');
parametro('id', 1);
parametro('bool', false);
console.log(texto);