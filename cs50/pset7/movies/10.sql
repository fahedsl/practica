--In 10.sql, write a SQL query to list the names of all people who have directed a movie that received a rating of at least 9.0.
--    Your query should output a table with a single column for the name of each person.
--    If a person directed more than one movie that received a rating of at least 9.0, they should only appear in your results once.

select distinct p.name
from movies as m
join ratings as r
	on m.id = r.movie_id
join directors as d
	on m.id = d.movie_id
join people as p
	on d.person_id = p.id
where r.rating >= 9.0
