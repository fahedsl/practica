def main():
    retorno = "INVALID"
    entrada = input("Number: ")
    codigoTamano = len(str(entrada))

    if codigoTamano == 13 or codigoTamano == 15 or codigoTamano == 16:

        codigo = list(map(int, entrada))

        if codigo[0] == 3 and (codigo[1] == 4 or codigo[1] == 7):
            retorno = "AMEX"
        elif codigo[0] == 5 and (codigo[1] == 1 or codigo[1] == 2 or codigo[1] == 3 or codigo[1] == 4 or codigo[1] == 5):
            retorno = "MASTERCARD"
        elif codigo[0] == 4:
            retorno = "VISA"

        if retorno != "INVALID":
            if not luhn(codigo):
                retorno = "INVALID"

    print(retorno)


def luhn(codigo):
    retorno = False
    suma = 0

    for i in range(len(codigo) - 2, -1, -2):
        if codigo[i] * 2 > 9:
            suma = suma + 1 + ((codigo[i] * 2) % 10)
        else:
            suma = suma + (codigo[i] * 2)
    for i in range(len(codigo) - 1, -1, -2):
        suma = suma + codigo[i]

    if (suma % 10 == 0):
        retorno = True

    return retorno


main()