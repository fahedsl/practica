function ajax(contenido, metodo='GET', callback){
	let datos = null;
	let pedido = new XMLHttpRequest();

	function error(error){
		console.log(error);
	}

	pedido.open(metodo, contenido);
	
	pedido.send();

	pedido.onload = function(){
		if (pedido.status >= 200 && pedido.status < 400) {
			datos = pedido.responseText;
			callback(datos);
		}
		else{
			let mensaje = 'No se encontró el archivo.';
			error(mensaje);
		}
	}

	pedido.onerror = function(){
		let mensaje = 'Error de conexion';
		error(mensaje);
	}

	return datos;
}

function boton1(){
	ajax(
		'http://node.local/incidencias/123/0/100/eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6W3sidXN1YXJpb0lkIjoxfV0sImV4cCI6MTY0MTkzNDgyMH0.x9-I8c9S4g_z8jciWGIh1QoF_34R2vAQz2gyfP2Tb4w'
		,'GET'
		, function(data){
			console.log('datos: ',data);
		}
	);
}
