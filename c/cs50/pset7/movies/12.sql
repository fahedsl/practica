--In 12.sql, write a SQL query to list the titles of all movies in which both Johnny Depp and Helena Bonham Carter starred.
--    Your query should output a table with a single column for the title of each movie.
--    You may assume that there is only one person in the database with the name Johnny Depp.
--    You may assume that there is only one person in the database with the name Helena Bonham Carter.

select h.*
from 
	(select m.title
	from movies as m
	join stars as s
	on m.id = s.movie_id
	join people as p
	on s.person_id = p.id
	where p.name = 'Johnny Depp') j
join
	(select title
	from movies as m
	join stars as s
	on m.id = s.movie_id
	join people as p
	on s.person_id = p.id
	where p.name = 'Helena Bonham Carter') h
	on j.title = h.title;
