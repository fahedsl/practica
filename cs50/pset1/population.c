#include <stdio.h>
#include <math.h>
#include <cs50.h>

int calcularPoblacionVariacionPeriodo(int poblacionInicial, int poblacionFinal, float tasaNatalidad, float tasaMortalidad);
int calcularPoblacionVariacionAnual(int poblacionInicial, float tasaNatalidad, float tasaMortalidad);


int main(void)
{
    int poblacionInicial;
    int poblacionFinal;
    float tasaNatalidad = 1.0 / 3; // 0.33
    float tasaMortalidad = 1.0 / 4; // 0.25
    int tiempoTranscurrido = 0;
    int poblacionMinima = 9;

    do
    {
        poblacionInicial = get_int("Población inicial: ");
    }
    while (poblacionInicial < poblacionMinima);

    do
    {
        poblacionFinal = get_int("Población final: ");
    }
    while (poblacionFinal < poblacionInicial);

    tiempoTranscurrido = calcularPoblacionVariacionPeriodo(poblacionInicial, poblacionFinal, tasaNatalidad, tasaMortalidad);

    printf("Years: %i", tiempoTranscurrido);
}


int calcularPoblacionVariacionPeriodo(int poblacionInicial, int poblacionFinal, float tasaNatalidad, float tasaMortalidad)
{
    int poblacionTemporal = poblacionInicial;
    int tiempoTranscurridoLocal = 0;

    while (poblacionTemporal < poblacionFinal)
    {
        int poblacionVariacion = calcularPoblacionVariacionAnual(poblacionTemporal, tasaNatalidad, tasaMortalidad);
        poblacionTemporal += poblacionVariacion;
        tiempoTranscurridoLocal++;
    }

    return tiempoTranscurridoLocal;
}


int calcularPoblacionVariacionAnual(int poblacionInicial, float tasaNatalidad, float tasaMortalidad)
{
    float aumento = floor((float)poblacionInicial * tasaNatalidad);
    float disminucion = floor((float)poblacionInicial * tasaMortalidad);
    int variacion = aumento - disminucion;
    return variacion;
}