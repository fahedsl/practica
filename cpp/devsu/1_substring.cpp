#include <string>

using namespace std;

int main()
{
	string texto1, texto2;
	int texto1Size, texto2Size, max;
	int encontradosTamanoMax = 100;
	int item = 0;
	int encontrados[encontradosTamanoMax];
	int encontradoUltimo = -1;

	// entradas
	printf("text = ");
	{
		char temp[1000];
		scanf("%999[^\n]", temp);
		texto1 = temp;
	}
	printf("subText = ");
	{
		char temp[1000];
		scanf("%999[^\n]", temp);
		texto2 = temp;
	}

	// valores
	texto1Size= texto1.length();
	texto2Size= texto2.length();
	max = texto1Size;
	for(int i = 0; i < encontradosTamanoMax; i++){
		encontrados[i] = -1;
	}

	// encontrados
	while(encontradoUltimo < texto1Size){
		encontradoUltimo++;
		int encontrado = texto1.find(texto2, encontradoUltimo);
		if (encontrado != -1){
			encontrados[item] = encontrado;
			encontradoUltimo = encontrado;
			item++;
		}
		else{
			break;
		}
	}

	//find minimum
	for(int i = 0; i <= item; i++){
		if(encontrados[i] > max){
			max = encontrados[i];
		}

		int temp = texto1Size - (encontrados[i] + texto2Size);
		if(temp > max){
			max = temp;
		}
	}

	printf("%i", max);

	return 0;
}