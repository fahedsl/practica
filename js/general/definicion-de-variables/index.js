'use strict';

function a(){
	console.log('A: ', this);
	
	function b(){
		console.log('B: ', this);
	}
	b();
}
a();


let c = {
	n1: 'test',
	c1: function() {
		console.log('C: ', this);
		function d1() {
			console.log('D: ', this);
		}
		d1();
	}
}
try{
	c1();
}catch{console.log('nadaa');};
c.c1();
/*
let e = new c();
e.c1();
*/