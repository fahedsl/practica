function x(){}

let cb1 = function(){console.log('something')};

function o(cb){
	x()
	cb();
}

setTimeout(
	o(cb1)
	,100
);

function C (callback){
	function c(callback1){
		x()
		callback1();
	}
	setTimeout(c(callback), 100);
}

function D (callback){
	setTimeout(function d(callback){
		x()
		callback();
	}, 100);
}

function E (callback){
	let callback1 = callback;
	setTimeout(function e(callback1){
		x()
		callback1();
	}, 100);
}

A( function log(){console.log('Print A')});
B( function log(){console.log('Print B')});
C( function log(){console.log('Print C')});
D( function log(){console.log('Print D')});
E( function log(){console.log('Print E')});
