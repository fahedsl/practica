--In 8.sql, write a SQL query to list the names of all people who starred in Toy Story.
--    Your query should output a table with a single column for the name of each person.
--    You may assume that there is only one movie in the database with the title Toy Story.

select p.name
from stars as s
join movies as m
	on m.id = s.movie_id
join people as p
	on p.id = s.person_id
where m.title = 'Toy Story';
